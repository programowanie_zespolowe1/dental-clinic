package main.java.GUI.patientList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import main.java.database.entity.Profile;
import main.java.entry.AppSession;
import main.java.navigator.Path;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable{

    @FXML
    private TableView<Profile> patientsList;

    @FXML
    private TableColumn<Profile, String> lastName;

    @FXML
    private TableColumn<Profile, String> firstName;

    @FXML
    private TableColumn<Profile, String> PESEL;



    @FXML
    private Button editPatientBtn;

    @FXML
    private Button deletePatientBtn;

    @FXML
    private Button patientCardBtn;

    @FXML
    private Button makeAppointmentBtn;

    @FXML
    private Button backBtn;

    @FXML
    private Button logoutBtn;

    @FXML
    private Button homeBtn;

    @FXML
    private Button confirmBtn;

    @FXML
    private TextField searchPanel;

    @FXML
    private Text firstNameDisplay;

    @FXML
    private Text lastNameDisplay;

    @FXML
    private Text peselDisplay;

    @FXML
    private Text phoneDisplay;

    @FXML
    private Text emailDisplay;

    @FXML
    private Text cityDisplay;

    @FXML
    private Text addressDisplay;

    @FXML
    private Text postalCodeDisplay;


    Service service = new Service();

    @FXML
    void backAction(ActionEvent event) {

        service.navigateToPath(Path.landing);
    }

    @FXML
    void editPatientAction(ActionEvent event) {

        AppSession.getInstance().setEditFromPatientsList(patientsList.getSelectionModel().getSelectedItem());
        service.navigateToPath(Path.editPatient);
    }

    @FXML
    void deletePatientAction(ActionEvent event) {


        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Czy jesteś pewny/a, że chcesz usunąć"
                + " tego pacjenta?", ButtonType.YES, ButtonType.CANCEL);
        alert.setHeaderText("Usuwanie pacjenta");
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            service.deletePatient(patientsList.getSelectionModel().getSelectedItem());
            patientsList.getItems().clear();
            patientsList.getItems().addAll(service.loadAllVariables());

        }
    }

    @FXML
    void homeAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    @FXML
    void makeAppointmentAction(ActionEvent event) {
        service.navigateToPath(Path.makeAppointment);

        AppSession.getInstance().setPatientForVisit(patientsList.getSelectionModel().getSelectedItem());

        AppSession.getInstance().getNavigator().navigateTo(Path.makeAppointment);
    }

    /**
     * Search for patient by typed String
     *
     * @param event
     */
    @FXML
    void searchFromPatientsListAction(KeyEvent event) {
        if (!event.getCode().equals(KeyCode.ENTER)) {
            return;
        }

        patientsList.getItems().clear();

        var searchString = searchPanel.getText().toLowerCase();
        var patientList = service.loadAllVariables();
        var filteredList = new ArrayList<Profile>();

        for (var p: patientList) {
            if (p.getLastName().toLowerCase().contains(searchString)) {
                filteredList.add(p);
            }
            else if (p.getFirstName().toLowerCase().contains(searchString)) {
                filteredList.add(p);
            }
            else if (p.getPESEL().toLowerCase().contains(searchString)) {
                filteredList.add(p);
            }
        }

        patientsList.getItems().addAll(filteredList);

    }


    @FXML
    void patientCardAction(ActionEvent event) {
        AppSession.getInstance().setChoosenForToothHistory(patientsList.getSelectionModel().getSelectedItem());
        service.navigateToPath(Path.patientCard);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        PESEL.setCellValueFactory(new PropertyValueFactory<>("PESEL"));

        patientsList.getItems().addAll(service.loadAllVariables());

        patientsList.setRowFactory( tv -> {
            TableRow<Profile> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if ((! row.isEmpty())) {
                    Profile rowData = row.getItem();
                    loadPatientDetailsOnRightDisplay(rowData);
                }
            });
            return row;
        });

    }

    /**
     * Loads patient details on right pane
     *
     * @param profile
     */
    public void loadPatientDetailsOnRightDisplay(Profile profile){

        firstNameDisplay.setText(patientsList.getSelectionModel().getSelectedItem().getFirstName());
        lastNameDisplay.setText(patientsList.getSelectionModel().getSelectedItem().getLastName());
        peselDisplay.setText(patientsList.getSelectionModel().getSelectedItem().getPESEL());
        phoneDisplay.setText(patientsList.getSelectionModel().getSelectedItem().getPhone());
        emailDisplay.setText(patientsList.getSelectionModel().getSelectedItem().getEmail());
        cityDisplay.setText(patientsList.getSelectionModel().getSelectedItem().getCity());
        addressDisplay.setText(patientsList.getSelectionModel().getSelectedItem().getAddress());
        postalCodeDisplay.setText(patientsList.getSelectionModel().getSelectedItem().getPostalCode());
    }

}
