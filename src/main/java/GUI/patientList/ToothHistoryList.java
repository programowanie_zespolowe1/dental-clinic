package main.java.GUI.patientList;

import java.util.Date;

public class ToothHistoryList {
    public Date date;
    public String description;

    public ToothHistoryList(Date date, String desc) {
        this.date = date;
        this.description = desc;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }
}
