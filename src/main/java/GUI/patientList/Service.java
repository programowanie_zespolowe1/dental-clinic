package main.java.GUI.patientList;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.ToothHistory;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

import main.java.entry.AppSession;
import main.java.navigator.Path;

public class Service {

    /**
     *  Navigates to given FXML path using Paths enum.
     *
     *  @param path Path enum to FXML file
     */
    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }

    public List<Profile> loadAllVariables() {

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);

            userProfileCriteriaQuery.select(userProfileRoot).where(builder.isNull(userProfileRoot.get("userGuid")));
            List<Profile> userProfileList = session.createQuery(userProfileCriteriaQuery).getResultList();
            return userProfileList;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    public void deletePatient(Profile profile){

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);
            CriteriaQuery<ToothHistory> userToothHistoryCriteriaQuery = builder.createQuery(ToothHistory.class);
            Root<ToothHistory> userToothHistoryRoot = userToothHistoryCriteriaQuery.from(ToothHistory.class);

            userProfileCriteriaQuery.select(userProfileRoot).where(builder.equal(userProfileRoot.get("PESEL"), profile.getPESEL()));
            Profile userProfile = (Profile) session.createQuery(userProfileCriteriaQuery).getSingleResult();

            userToothHistoryCriteriaQuery.select(userToothHistoryRoot).where(builder.equal(userToothHistoryRoot.get("profile"), profile));
            List<ToothHistory> toothHistory = session.createQuery(userToothHistoryCriteriaQuery).getResultList();

            for (ToothHistory th: toothHistory) {
                session.delete(th);
            }
            session.delete(userProfile);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }
}
