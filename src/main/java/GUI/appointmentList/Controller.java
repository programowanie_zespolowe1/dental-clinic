package main.java.GUI.appointmentList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import main.java.database.entity.Profile;
import main.java.database.entity.Visit;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import main.java.reports.SampleReport;
import main.java.reports.VisitReport;

import java.io.FileNotFoundException;
import java.net.URL;
import java.sql.Time;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private Text dateField;

    @FXML
    private Text patientNameField;

    @FXML
    private Text patientLastNameField;

    @FXML
    private Text patientPeselField;

    @FXML
    private Text patientPhoneField;

    @FXML
    private Text patientCityField;

    @FXML
    private Text patientStreetField;

    @FXML
    private Text patientPostalCodeField;

    @FXML
    private Text patientDentistField;

    @FXML
    private TableView<Visit> visitList;

    @FXML
    private TableColumn<Visit, Time> visitDate;


    private Service service = new Service();

    private SampleReport report = new SampleReport();

    private VisitReport visitReport = new VisitReport();

    @FXML
    void backAction(ActionEvent event) { service.navigateToPath(Path.calendar); }

    @FXML
    void homeAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }


    @FXML
    void deleteVisitAction(ActionEvent event) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Czy jesteś pewny/a, że chcesz usunąć"
                + " tę wizytę?", ButtonType.YES, ButtonType.CANCEL);
        alert.setHeaderText("Usuwanie wizyty");
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            service.deleteVisit(visitList.getSelectionModel().getSelectedItem());
            visitList.getItems().clear();
            loadVisitList();
        }

    }

    @FXML
    void editVisitAction(ActionEvent event) {

        service.deleteVisit(visitList.getSelectionModel().getSelectedItem());
        AppSession.getInstance().setPatientForVisit(visitList.getSelectionModel().getSelectedItem().getPatientProfile());

        service.navigateToPath(Path.makeAppointment);

    }


    @FXML
    void generateCardReportAction(ActionEvent event) throws FileNotFoundException {

        if(!(service.loadVisists(AppSession.getInstance().getDentistForCalendar(),
                AppSession.getInstance().getAppointmentDate()).isEmpty())){
            Profile userProfile = service.getProfileForUser(AppSession.getInstance().getLoggedUser());
            visitReport.generateVisitReport(userProfile.getFirstName() + " " + userProfile.getLastName());
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Wygenerowano raport");
            alert.showAndWait();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Nie można wygenerować raportu");
            alert.setContentText("Nie ma żadnych wizyt na dany dzień");
            alert.showAndWait();
        }

    }

    /**
     * Displays list of visits for dentist (saved in AppSession) in table and sets action which shows
     * patient details for visit after mouse clicked on table row
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        dateField.setText(String.valueOf(AppSession.getInstance().getAppointmentDate()));

        visitDate.setCellValueFactory(new PropertyValueFactory<>("visitTime"));

        loadVisitList();

    }

    public void loadVisitList(){

        visitList.getItems().addAll(service.loadVisists(AppSession.getInstance().getDentistForCalendar(),
                AppSession.getInstance().getAppointmentDate()));
        visitList.setRowFactory( tv -> {
            TableRow<Visit> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if ((! row.isEmpty()) ) {
                    Visit rowData = row.getItem();
                    loadPatientDetails(rowData);
                }
            });
            return row ;
        });
    }

    public void loadPatientDetails(Visit visit){

        Profile patientProfile = visit.getPatientProfile();
        patientNameField.setText(patientProfile.getFirstName());
        patientLastNameField.setText(patientProfile.getLastName());
        patientPeselField.setText(patientProfile.getPESEL());
        patientPhoneField.setText(patientProfile.getPhone());
        patientCityField.setText(patientProfile.getCity());
        patientStreetField.setText(patientProfile.getAddress());
        patientPostalCodeField.setText(patientProfile.getPostalCode());
        patientDentistField.setText(visit.getDentistProfile().getFirstName() + " " + visit.getDentistProfile().getLastName());
    }

}
