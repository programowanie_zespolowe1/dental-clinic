package main.java.GUI.appointmentList;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.User;
import main.java.database.entity.Visit;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;

public class Service {

    /**
     * Gets all visits with given dentist and date from database.
     *
     * @param dentistProfile   dentist Profile
     * @param date              visit date
     * @return list of visits for dentist and date
     */
    public List<Visit> loadVisists(Profile dentistProfile, LocalDate date) {

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Visit> visitCriteriaQuery = builder.createQuery(Visit.class);
            Root<Visit> visitRoot = visitCriteriaQuery.from(Visit.class);

            Predicate[] predicates = new Predicate[2];
            predicates[0] = builder.equal(visitRoot.get("visitDate"), date);
            predicates[1] = builder.equal(visitRoot.get("dentistProfile"), dentistProfile);

            visitCriteriaQuery.select(visitRoot).where(predicates);
            Query sessionFindVisitQuery = session.createQuery(visitCriteriaQuery);
            List<Visit> visitsList = sessionFindVisitQuery.getResultList();
            return visitsList;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    public void deleteVisit(Visit visit){

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Visit> visitCriteriaQuery = builder.createQuery(Visit.class);
            Root<Visit> visitRoot = visitCriteriaQuery.from(Visit.class);

            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(visitRoot.get("visitDate"), visit.getVisitDate());
            predicates[1] = builder.equal(visitRoot.get("visitTime"), visit.getVisitTime());
            predicates[2] = builder.equal(visitRoot.get("dentistProfile"), visit.getDentistProfile());
            predicates[3] = builder.equal(visitRoot.get("patientProfile"), visit.getPatientProfile());

            visitCriteriaQuery.select(visitRoot).where(predicates);
            Visit foundVisit = (Visit) session.createQuery(visitCriteriaQuery).getSingleResult();

            session.delete(foundVisit);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    public Profile getProfileForUser(User user){
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);

            userProfileCriteriaQuery.select(userProfileRoot).where(builder.equal(userProfileRoot.get("userGuid"), user));
            Profile profileForUser = (Profile) session.createQuery(userProfileCriteriaQuery).getSingleResult();

            return profileForUser;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }


    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }
}
