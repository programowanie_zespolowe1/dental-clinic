package main.java.GUI.addToothHistory;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.ToothHistory;
import main.java.database.entity.User;
import main.java.database.enums.Role;
import main.java.database.enums.ToothState;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.sql.Date;

public class Service {

    public void navigateToPath(main.java.navigator.Path path) {
        main.java.entry.AppSession appSession = main.java.entry.AppSession.getInstance();
        try {
            appSession.getNavigator().navigateTo(path);
        } catch (Exception e) {
            throw e;
        }
    }

    public void addToothHistoryToPatient(Integer toothNumber, Date visitDate, String history, String xRayPicture,
                                                ToothState toothState, Profile profile) {

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            ToothHistory toothHistory = new ToothHistory(toothNumber, visitDate, history, xRayPicture, toothState, profile);
            session.save(toothHistory);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}
