package main.java.GUI.addToothHistory;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.scene.text.Text;
import main.java.database.entity.ToothHistory;
import main.java.database.enums.ToothState;
import main.java.entry.AppSession;
import main.java.navigator.Path;

import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

public class Controller {

    @FXML
    private Text toothNumberField;

    @FXML
    private DatePicker calendarField;

    @FXML
    private ChoiceBox<String> toothStateField;

    @FXML
    private TextField xRayPictureField;

    @FXML
    private TextArea historyField;

    @FXML
    private Button confirmBtn;

    @FXML
    private Button backBtn;

    @FXML
    private Button logoutBtn;

    @FXML
    private Button homeBtn;

    private Service service = new Service();

    @FXML
    void backAction(ActionEvent event) {
        AppSession appSession = AppSession.getInstance();

        appSession.getNavigator().navigateTo(main.java.navigator.Path.patientList);
    }

    @FXML
    void confirmAction(ActionEvent event) {
            service.addToothHistoryToPatient(AppSession.getInstance().getToothNumber(), Date.valueOf(calendarField.getValue()),
                    historyField.getText(),xRayPictureField.getText(), ToothState.valueOf(toothStateField.getValue()),
                    AppSession.getInstance().getChoosenForToothHistory());

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Dodano historię leczenia");
            alert.showAndWait();

            service.navigateToPath(Path.patientCard);

    }

    @FXML
    void homeAction(ActionEvent event) {
        AppSession appSession = AppSession.getInstance();

        appSession.getNavigator().navigateTo(main.java.navigator.Path.landing);
    }



    @FXML
    void logoutAction(ActionEvent event) {
        AppSession appSession = AppSession.getInstance();

        appSession.getNavigator().navigateTo(Path.login);
    }

}
