package main.java.GUI.patientCard;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.ToothHistory;
import main.java.database.entity.User;
import main.java.database.enums.Role;
import main.java.database.enums.ToothState;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

public class Service {

    /**
     *  Navigates to given FXML path using Paths enum.
     *
     *  @param path Path enum to FXML file
     */
    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }

    public void showToothHistory(ToothHistory toothHistory){

    }

    public void editToothHistory(){

    }


    public void generateTreatmentHistory(){

    }

    /**
     * Loads history of treatment for given tooth
     *
     * @param toothNumber
     * @return
     */
    public List<ToothHistory> loadToothHistory(Integer toothNumber){
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<ToothHistory> toothHistoryCriteriaQuery = builder.createQuery(ToothHistory.class);
            Root<ToothHistory> toothHistoryRoot = toothHistoryCriteriaQuery.from(ToothHistory.class);


            Predicate[] predicates = new Predicate[2];
            predicates[0] = builder.equal(toothHistoryRoot.get("toothNumber"), toothNumber);
            predicates[1] = builder.equal(toothHistoryRoot.get("profile"), AppSession.getInstance().getChoosenForToothHistory());

            toothHistoryCriteriaQuery.select(toothHistoryRoot).where(predicates);

            return session.createQuery(toothHistoryCriteriaQuery).getResultList();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        }finally {
            session.close();
        }
    }

    public ToothHistory getToothHistoryForEdit(String history, int toothNumber, Date visitDate, Profile profile){
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<ToothHistory> toothHistoryCriteriaQuery = builder.createQuery(ToothHistory.class);
            Root<ToothHistory> toothHistoryRoot = toothHistoryCriteriaQuery.from(ToothHistory.class);

            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(toothHistoryRoot.get("toothNumber"), toothNumber);
            predicates[1] = builder.equal(toothHistoryRoot.get("visitDate"), visitDate);
            predicates[2] = builder.equal(toothHistoryRoot.get("history"), history);
            predicates[3] = builder.equal(toothHistoryRoot.get("profile"), profile);

            toothHistoryCriteriaQuery.select(toothHistoryRoot).where(predicates);
            ToothHistory foundToothHistory = (ToothHistory) session.createQuery(toothHistoryCriteriaQuery).getSingleResult();

            return foundToothHistory;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }
}
