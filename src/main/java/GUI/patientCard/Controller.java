package main.java.GUI.patientCard;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Pair;
import main.java.GUI.patientCard.Service;
import main.java.GUI.patientList.ToothHistoryList;
import main.java.database.entity.ToothHistory;
import main.java.database.entity.comparators.ToothHistoryDateComparator;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import main.java.reports.CardReport;
import main.java.reports.SampleReport;
import main.java.reports.ToothHistoryReport;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private Button t11 = new Button();

    @FXML
    private Button t12 = new Button();

    @FXML
    private Button t13 = new Button();

    @FXML
    private Button t14 = new Button();

    @FXML
    private Button t15 = new Button();

    @FXML
    private Button t16 = new Button();

    @FXML
    private Button t17 = new Button();

    @FXML
    private Button t18 = new Button();

    @FXML
    private Button t21 = new Button();

    @FXML
    private Button t22 = new Button();

    @FXML
    private Button t23 = new Button();

    @FXML
    private Button t24 = new Button();

    @FXML
    private Button t25 = new Button();

    @FXML
    private Button t26 = new Button();

    @FXML
    private Button t27 = new Button();

    @FXML
    private Button t28 = new Button();

    @FXML
    private Button t31 = new Button();

    @FXML
    private Button t32 = new Button();

    @FXML
    private Button t33 = new Button();

    @FXML
    private Button t34 = new Button();

    @FXML
    private Button t35 = new Button();

    @FXML
    private Button t36 = new Button();

    @FXML
    private Button t37 = new Button();

    @FXML
    private Button t38 = new Button();

    @FXML
    private Button t41 = new Button();

    @FXML
    private Button t42 = new Button();

    @FXML
    private Button t43 = new Button();

    @FXML
    private Button t44 = new Button();

    @FXML
    private Button t45 = new Button();

    @FXML
    private Button t46 = new Button();

    @FXML
    private Button t47 = new Button();

    @FXML
    private Button t48 = new Button();

    @FXML
    private Button editToothHistoryBtn;

    @FXML
    private Button addToothHistoryBtn;

    @FXML
    private TableView<ToothHistoryList> toothHistoryList;

    @FXML
    private TableColumn<ToothHistoryList, Date> visitDate;

    @FXML
    private TableColumn<ToothHistoryList, String> visitDescription;

    @FXML
    private Button backBtn2;

    @FXML
    private Button logoutBtn;

    @FXML
    private Button homeBtn;

    @FXML
    private Button generateToothHistoryReportBtn;

    @FXML
    private Button generateCardReportBtn;

    @FXML
    private Button backBtn;

    @FXML
    private Button upperTeethBtn;

    @FXML
    private Button lowerTeethBtn;

    Service service = new Service();
    SampleReport report = new SampleReport();
    ToothHistoryReport toothHistoryReport = new ToothHistoryReport();
    CardReport cardReport = new CardReport();

    @FXML
    void backAction(ActionEvent event) {
        service.navigateToPath(Path.patientList);
    }

    @FXML
    void editToothHistoryAction(ActionEvent event) {
        ToothHistory toothHistoryToEdit = service.getToothHistoryForEdit(
                toothHistoryList.getSelectionModel().getSelectedItem().getDescription(),
                AppSession.getInstance().getToothNumber(),
                toothHistoryList.getSelectionModel().getSelectedItem().getDate(),
                AppSession.getInstance().getChoosenForToothHistory()
                );

        AppSession.getInstance().setEditToToothHistory(toothHistoryToEdit);
        service.navigateToPath(Path.editToothHistory);
    }

    @FXML
    void addToothHistoryAction(ActionEvent event) {

        service.navigateToPath(Path.addToothHistory);

    }

    @FXML
    void homeAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    @FXML
    void lowerTeethAction(ActionEvent event) {
        AppSession.getInstance().setJaw("lower");
        service.navigateToPath(Path.lowerTeeth);
    }

    @FXML
    void upperTeethAction(ActionEvent event) {
        AppSession.getInstance().setJaw("upper");
        service.navigateToPath(Path.upperTeeth);
    }

    @FXML
    void backAction2(ActionEvent event) {
        service.navigateToPath(Path.patientCard);
    }

    /**
     * Generates patient's card report
     *
     * @param event
     * @throws FileNotFoundException
     */
    @FXML
    void generateCardReportAction(ActionEvent event) throws FileNotFoundException {
        cardReport.createPdf(AppSession.getInstance().getChoosenForToothHistory().getGUID());
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Wygenerowano raport");
        alert.showAndWait();
    }

    /**
     * Shows history of choosen tooth
     *
     * @param event
     */
    @FXML
    void showToothHistoryAction(ActionEvent event) {
        var btn = (Button) event.getSource();
        var btnNum = Integer.parseInt(btn.getText());

        toothHistoryList.getItems().clear();

        var thList = service.loadToothHistory(btnNum);
        AppSession.getInstance().setToothNumber(btnNum);
        var filteredList = new ArrayList<ToothHistoryList>();

        for (var th: thList) {
            filteredList.add(new ToothHistoryList(th.getVisitDate(), th.getHistory()));
        }

        toothHistoryList.getItems().addAll(filteredList);
    }

    /**
     * Generate report of treatment history for single tooth
     *
     * @param event
     * @throws FileNotFoundException
     */
    @FXML
    void generateToothHistoryReportAction(ActionEvent event) throws FileNotFoundException {
        toothHistoryReport.generateToothHistoryPdf(AppSession.getInstance().getChoosenForToothHistory().getGUID());
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Wygenerowano raport");
        alert.showAndWait();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (visitDate == null || visitDescription == null) {
            visitDate = new TableColumn<>();
            visitDescription = new TableColumn<>();
        }

        visitDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        visitDescription.setCellValueFactory(new PropertyValueFactory<>("description"));

        if(AppSession.getInstance().getJaw().equals("upper") || AppSession.getInstance().getJaw().equals("lower")){
            setTeethStatesView();
        }
    }

    void setTeethStatesView(){
        List<Pair<Integer, Button>> allTeethBtn = createListOfButtons();

        for (Pair<Integer, Button> numToothPair: allTeethBtn) {
            var thList = service.loadToothHistory(numToothPair.getKey());
            thList.sort(new ToothHistoryDateComparator());

            var th = thList.get(0);

            switch (th.getToothState()) {
                case GOOD -> numToothPair.getValue().setStyle("-fx-background-color: #2fff00;-fx-border-radius: 100; -fx-background-radius: 100;");
                case FOR_TREATMENT -> numToothPair.getValue().setStyle("-fx-background-color: #ff0000;-fx-border-radius: 100; -fx-background-radius: 100;");
                case REMOVED -> numToothPair.getValue().setStyle("-fx-background-color: #6e6e6e;-fx-border-radius: 100; -fx-background-radius: 100;");
                case IN_PROGRESS -> numToothPair.getValue().setStyle("-fx-background-color: #ffd500;-fx-border-radius: 100; -fx-background-radius: 100;");
                default -> numToothPair.getValue().setStyle("-fx-background-color: #ffffff;-fx-border-radius: 100; -fx-background-radius: 100;");
            }
        }
    }

    List<Pair<Integer, Button>> createListOfButtons() {
        List<Pair<Integer, Button>> list = new ArrayList<Pair<Integer, Button>>();

        if (AppSession.getInstance().getJaw().equals("upper")) {
            list.add(new Pair<>(11, t11));
            list.add(new Pair<>(12, t12));
            list.add(new Pair<>(13, t13));
            list.add(new Pair<>(14, t14));
            list.add(new Pair<>(15, t15));
            list.add(new Pair<>(16, t16));
            list.add(new Pair<>(17, t17));
            list.add(new Pair<>(18, t18));

            list.add(new Pair<>(21, t21));
            list.add(new Pair<>(22, t22));
            list.add(new Pair<>(23, t23));
            list.add(new Pair<>(24, t24));
            list.add(new Pair<>(25, t25));
            list.add(new Pair<>(26, t26));
            list.add(new Pair<>(27, t27));
            list.add(new Pair<>(28, t28));
        }

        if(AppSession.getInstance().getJaw().equals("lower")) {
            list.add(new Pair<>(31, t31));
            list.add(new Pair<>(32, t32));
            list.add(new Pair<>(33, t33));
            list.add(new Pair<>(34, t34));
            list.add(new Pair<>(35, t35));
            list.add(new Pair<>(36, t36));
            list.add(new Pair<>(37, t37));
            list.add(new Pair<>(38, t38));

            list.add(new Pair<>(41, t41));
            list.add(new Pair<>(42, t42));
            list.add(new Pair<>(43, t43));
            list.add(new Pair<>(44, t44));
            list.add(new Pair<>(45, t45));
            list.add(new Pair<>(46, t46));
            list.add(new Pair<>(47, t47));
            list.add(new Pair<>(48, t48));
        }
        return list;
    }


}
