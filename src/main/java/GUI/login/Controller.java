package main.java.GUI.login;

import main.java.database.Connector;
import main.java.database.entity.User;
import org.hibernate.Session;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import main.java.navigator.Path;
import java.sql.SQLException;


public class Controller {

    @FXML
    private Button loginBtn;
    @FXML
    private TextField usernameTextField;
    @FXML
    private PasswordField enterPasswordField;
    @FXML
    private Label loginMessageLabel;



    public void loginBtnOnAction(ActionEvent event) throws SQLException {
        if (!usernameTextField.getText().isBlank() && !enterPasswordField.getText().isBlank()) {
            validateLogin();
        } else {
            loginMessageLabel.setText("");
            loginMessageLabel.setText("Proszę wypełnić puste pola!");
        }
    }

    /**
     * Validation for login
     *
     * @throws SQLException
     */
    private void validateLogin() throws SQLException {

        Connector.openSession();
        Session currentSession = Connector.getSessionFactory().getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<User> userCriteriaQuery = builder.createQuery(User.class);
        Root<User> userRoot = userCriteriaQuery.from(User.class);

        try {
            Predicate[] predicates = new Predicate[2];
            predicates[0] = builder.equal(userRoot.get("userName"), usernameTextField.getText());
            predicates[1] = builder.equal(userRoot.get("password"), enterPasswordField.getText());

            userCriteriaQuery.select(userRoot).where(predicates);
            Query sessionFindUserQuery = currentSession.createQuery(userCriteriaQuery);
            User foundOriginalUser = null;
            try {
                foundOriginalUser = (User) sessionFindUserQuery.getSingleResult();
            } catch (Exception e) {
                loginMessageLabel.setText("Nieprawidłowa nazwa użytkownika lub hasło. Spróbuj ponownie!");
            }

            currentSession.getTransaction().commit();

            if (foundOriginalUser != null) {
                main.java.entry.AppSession appSession = main.java.entry.AppSession.getInstance();
                appSession.setLoggedUser(foundOriginalUser);
                appSession.getNavigator().navigateTo(Path.landing);
            }
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

          throw  e;
        } finally {
            currentSession.close();
        }
    }
}

