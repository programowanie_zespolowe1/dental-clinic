package main.java.GUI.userInfo;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.Schedule;
import main.java.database.entity.User;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Service {

    /**
     * Gets Profile from database for logged User.
     *
     * @param user      User logged user
     * @return profile for user
     */
    public Profile getProfileForUser(User user){
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);

            userProfileCriteriaQuery.select(userProfileRoot).where(builder.equal(userProfileRoot.get("userGuid"), user));
            Profile profileForUser = (Profile) session.createQuery(userProfileCriteriaQuery).getSingleResult();

            return profileForUser;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Gets Schedule from database for logged User and changes names of the days of the week
     * from english to polish
     *
     * @param profile user Profile
     * @return lists of schedules for user.
     */
    public List<Schedule> getScheduleForUser(Profile profile) {
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Schedule> scheduleCriteriaQuery = builder.createQuery(Schedule.class);
            Root<Schedule> scheduleRoot = scheduleCriteriaQuery.from(Schedule.class);

            scheduleCriteriaQuery.select(scheduleRoot).where(builder.equal(scheduleRoot.get("profile"), profile));
            List<Schedule> scheduleList = session.createQuery(scheduleCriteriaQuery).getResultList();

            if(scheduleList != null) {

                for (int i = 0; i < scheduleList.size(); i++) {

                    String dayOfWeek = scheduleList.get(i).getDayOfWeek();

                    Map<String, String> map = new HashMap<String, String>();

                    map.put("MONDAY", "Poniedziałek");
                    map.put("TUESDAY", "Wtorek");
                    map.put("WEDNESDAY", "Środa");
                    map.put("THURSDAY", "Czwartek");
                    map.put("FRIDAY", "Piątek");
                    map.put("SATURDAY", "Sobota");
                    map.put("SUNDAY", "Niedziela");

                    String current = map.get(dayOfWeek);

                    scheduleList.get(i).setDayOfWeek(current);

                }
            }

            return scheduleList;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     *  Navigates to given FXML path using Paths enum.
     *
     *  @param path Path enum to FXML file
     */
    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }
}
