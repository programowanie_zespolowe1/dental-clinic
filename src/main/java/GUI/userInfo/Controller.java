package main.java.GUI.userInfo;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import main.java.database.entity.Profile;
import main.java.database.entity.Schedule;
import main.java.database.entity.User;
import main.java.entry.AppSession;
import main.java.navigator.Path;

import java.net.URL;
import java.sql.Time;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private TableView<Schedule> scheduleTable;

    @FXML
    private TableColumn<Schedule, String> dayOfWeekColumn;

    @FXML
    private TableColumn<Schedule, Time> fromTimeColumn;

    @FXML
    private TableColumn<Schedule, Time> toTimeColumn;

    @FXML
    private Text nameText;

    @FXML
    private Text lastNameText;

    @FXML
    private Text peselText;

    @FXML
    private Text phoneText;

    @FXML
    private Text cityText;

    @FXML
    private Text addressText;

    @FXML
    private Text postalCodeText;

    @FXML
    private Text emailText;

    private Service service = new Service();

    @FXML
    void backAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void editDetailsAction(ActionEvent event) {
        service.navigateToPath(Path.editUserInfo);

    }

    @FXML
    void editTimetableAction(ActionEvent event) {
        service.navigateToPath(Path.editTimetable);

    }

    @FXML
    void homeAction(ActionEvent event) {
       service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    public void displayUserInfo(){

        AppSession appSession = AppSession.getInstance();

        User loggedUser = appSession.getLoggedUser();
        Profile profileForUser = service.getProfileForUser(loggedUser);
        appSession.setLoggedUserProfile(profileForUser);


        nameText.setText(profileForUser.getFirstName());
        lastNameText.setText(profileForUser.getLastName());
        peselText.setText(profileForUser.getPESEL());
        phoneText.setText(profileForUser.getPhone());
        cityText.setText(profileForUser.getCity());
        addressText.setText(profileForUser.getAddress());
        postalCodeText.setText(profileForUser.getPostalCode());
        emailText.setText(profileForUser.getEmail());

        dayOfWeekColumn.setCellValueFactory(new PropertyValueFactory<>("dayOfWeek"));
        fromTimeColumn.setCellValueFactory(new PropertyValueFactory<>("startTime"));
        toTimeColumn.setCellValueFactory(new PropertyValueFactory<>("endTime"));

        scheduleTable.getItems().addAll(service.getScheduleForUser(profileForUser));

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        displayUserInfo();
    }
}
