package main.java.GUI.editUserInfo;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.User;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class Service {

    /**
     * Searches database for logged User (saved in AppSession)
     * Profile and updates it with given parameters.
     *
     * @param firstName     Profile first name
     * @param lastName      Profile last name
     * @param PESEL         Profile PESEL
     * @param phone         Profile phone
     * @param email         Profile email
     * @param city          Profile city
     * @param address       Profile address line 1
     * @param postalcode    Profile postal code
     */
    public void updateUser(String firstName, String lastName, String PESEL, String phone, String email, String city,
                           String address, String postalcode){
        Session session = Connector.openSession();
        Transaction tx = null;
        Profile userProfileForDetails = AppSession.getInstance().getLoggedUserProfile();
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);

            userProfileCriteriaQuery.select(userProfileRoot).where(builder.equal(userProfileRoot.get("PESEL"), userProfileForDetails.getPESEL()));
            Profile userProfile = (Profile) session.createQuery(userProfileCriteriaQuery).getSingleResult();

            userProfile.setFirstName(firstName);
            userProfile.setLastName(lastName);
            userProfile.setPESEL(PESEL);
            userProfile.setPhone(phone);
            userProfile.setEmail(email);
            userProfile.setCity(city);
            userProfile.setAddress(address);
            userProfile.setPostalCode(postalcode);

            session.getTransaction().commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Searches database for logged User (saved in AppSession) and updates his/her password.
     *
     * @param password      User password
     */
    public void updatePassword(String password){
        Session session = Connector.openSession();
        Transaction tx = null;

        Profile userProfileForDetails = AppSession.getInstance().getLoggedUserProfile();

        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);
            CriteriaQuery<User> userCriteriaQuery = builder.createQuery(User.class);
            Root<User> userRoot = userCriteriaQuery.from(User.class);

            userProfileCriteriaQuery.select(userProfileRoot).where(builder.equal(userProfileRoot.get("PESEL"), userProfileForDetails.getPESEL()));
            Profile userProfile = (Profile) session.createQuery(userProfileCriteriaQuery).getSingleResult();

            userCriteriaQuery.select(userRoot).where(builder.equal(userRoot.get("profile"), userProfile));
            User user = (User) session.createQuery(userCriteriaQuery).getSingleResult();

            user.setPassword(password);

            session.getTransaction().commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     *  Navigates to given FXML path using Paths enum.
     *
     *  @param path Path enum to FXML file
     */
    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }
}
