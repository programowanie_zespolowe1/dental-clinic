package main.java.GUI.editUserInfo;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.java.database.entity.Profile;
import main.java.entry.AppSession;
import main.java.navigator.Path;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class Controller implements Initializable {

    @FXML
    private TextField firstNameField;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField peselField;

    @FXML
    private TextField cityField;

    @FXML
    private TextField addressField;

    @FXML
    private TextField postalCodeField;

    @FXML
    private TextField phoneField;

    @FXML
    private TextField emailField;

    @FXML
    private Label messageLabel;

    @FXML
    private TextField passwordField;

    @FXML
    private TextField repeatPasswordField;

    private Service service = new Service();

    @FXML
    public void backAction(ActionEvent event) {
        service.navigateToPath(Path.userInfo);

    }

    @FXML
    public void confirmAction(ActionEvent event) {

        if (isInputValid()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Czy jesteś pewny/a, że chcesz zapisać"
                    + " te dane?", ButtonType.YES, ButtonType.CANCEL);
            alert.setHeaderText("Edytowanie danych");
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {

                updateUser();
                service.navigateToPath(Path.userInfo);
            }
        }
    }

    @FXML
    public void homeAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    public void logoutAction(ActionEvent event) {

        service.navigateToPath(Path.login);

    }

    @FXML
    public void changePasswordAction(ActionEvent event) {

        if (isPasswordValid()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Czy jesteś pewny/a, że chcesz zapisać"
                    + " to hasło?", ButtonType.YES, ButtonType.CANCEL);
            alert.setHeaderText("Zmienianie hasła");
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {

                service.updatePassword(passwordField.getText());
                passwordField.setText("");
                repeatPasswordField.setText("");
            }
        }
    }

    public void updateUser(){

        service.updateUser(firstNameField.getText(), lastNameField.getText(), peselField.getText(),
                phoneField.getText(), emailField.getText(), cityField.getText(), addressField.getText(), postalCodeField.getText());
    }

    private void setDetails(){
        Profile userForDetails = AppSession.getInstance().getLoggedUserProfile();
        firstNameField.setText(userForDetails.getFirstName());
        lastNameField.setText(userForDetails.getLastName());
        peselField.setText(userForDetails.getPESEL());
        phoneField.setText(userForDetails.getPhone());
        cityField.setText(userForDetails.getCity());
        addressField.setText(userForDetails.getAddress());
        postalCodeField.setText(userForDetails.getPostalCode());
        emailField.setText(userForDetails.getEmail());
    }

    private boolean isInputValid() {
        messageLabel.setText("");
        Pattern emailPattern = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)"
                + "*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")"
                + "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])"
                + "?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])"
                + "|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
        Pattern peselPattern = Pattern.compile("^\\d{11}$");
        Pattern phonePattern = Pattern.compile("(?<!\\w)(\\(?(\\+|00)?48\\)?)?[ -]?\\d{3}[ -]?\\d{3}[ -]?\\d{3}(?!\\w)");

        if(!(peselPattern.matcher(peselField.getText()).matches())){
            messageLabel.setText("Nieprawidłowy numer PESEL");
            return false;
        }
        else if(!(phonePattern.matcher(phoneField.getText()).matches())){
            messageLabel.setText("Nieprawidłowy numer telefonu");
            return false;
        }
        else if(!(emailField.getText().trim().isEmpty())){
            if(!(emailPattern.matcher(emailField.getText()).matches())){
                messageLabel.setText("Nieprawidłowy adres e-mail");
                return false;
            }
        }
        return true;
    }

    private boolean isPasswordValid(){
        messageLabel.setText("");

        Pattern passwordPattern = Pattern.compile("^\\w{6,}$");

        if(!(passwordPattern.matcher(passwordField.getText()).matches())){
            messageLabel.setText("Hasło musi zawierać min. 6 znaków");
            return false;
        }
        else if(!(repeatPasswordField.getText().equals(passwordField.getText()))){
            messageLabel.setText("Hasła muszą być takie same");
            return false;
        }
        return true;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setDetails();
    }

}
