package main.java.GUI.adminUserList;

import com.sun.scenario.effect.impl.sw.java.JSWBlend_SRC_OUTPeer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import main.java.GUI.adminUserList.Service;
import main.java.database.entity.Profile;
import main.java.entry.AppSession;
import main.java.navigator.Path;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private TableView<Profile> usersList;

    @FXML
    private TableColumn<Profile, String> name;

    @FXML
    private TableColumn<Profile, String> lastName;

    @FXML
    private TableColumn<Profile, String> pesel;

    @FXML
    private TableColumn<Profile, String> email;

    @FXML
    private TableColumn<Profile, String> phone;


    @FXML
    private Button addUserBtn;

    @FXML
    private Button backBtn;

    @FXML
    private Button logoutBtn;

    @FXML
    private Button homeBtn;

    private Service service = new Service();

    @FXML
    void addUserAction(ActionEvent event) {
        service.navigateToPath(Path.addUser);
    }

    @FXML
    void backAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void homeAction(ActionEvent event) {

        service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    @FXML
    void deleteUserAction(ActionEvent event) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Czy jesteś pewny/a, że chcesz usunąć"
                + " tego użytkownika?", ButtonType.YES, ButtonType.CANCEL);
        alert.setHeaderText("Usuwanie użytkownika");
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            service.deleteUser(usersList.getSelectionModel().getSelectedItem());
            usersList.getItems().clear();
            usersList.getItems().addAll(service.loadAllVariables());
        }

    }

    @FXML
    void detailsUserAction(ActionEvent event) {

        AppSession.getInstance().setUserForDetails(usersList.getSelectionModel().getSelectedItem());

        service.navigateToPath(Path.userDetails);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        name.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        pesel.setCellValueFactory(new PropertyValueFactory<>("PESEL"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        phone.setCellValueFactory(new PropertyValueFactory<>("phone"));

        usersList.getItems().addAll(service.loadAllVariables());

    }
}
