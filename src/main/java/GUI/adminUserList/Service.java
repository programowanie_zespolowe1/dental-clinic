package main.java.GUI.adminUserList;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.User;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

public class Service {

    /**
     * Gets all Profiles that have User from database.
     *
     * @return list of profiles that are users
     */
    public List<Profile> loadAllVariables() {

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);
            userProfileCriteriaQuery.select(userProfileRoot).where(builder.isNotNull(userProfileRoot.get("userGuid")));
            List<Profile> userProfileList = session.createQuery(userProfileCriteriaQuery).getResultList();
            return userProfileList;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Searches database for given Profile and its User and deletes them.
     *
     * @param profile Profile
     */
    public void deleteUser(Profile profile){

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);
            CriteriaQuery<User> userCriteriaQuery = builder.createQuery(User.class);
            Root<User> userRoot = userCriteriaQuery.from(User.class);

            userProfileCriteriaQuery.select(userProfileRoot).where(builder.equal(userProfileRoot.get("PESEL"), profile.getPESEL()));
            Profile userProfile = (Profile) session.createQuery(userProfileCriteriaQuery).getSingleResult();

            userCriteriaQuery.select(userRoot).where(builder.equal(userRoot.get("profile"), profile));
            User user = (User) session.createQuery(userCriteriaQuery).getSingleResult();

            session.delete(user);
            session.delete(userProfile);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     *  Navigates to given FXML path using Paths enum.
     *
     *  @param path Path enum to FXML file
     */
    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }

}
