package main.java.GUI.addPatient;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.ToothHistory;
import main.java.database.entity.User;
import main.java.database.enums.Role;
import main.java.database.enums.ToothState;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Service {

    /**
     * @return List of Profiles
     *
     * Load all dentists from database.
     *
     */
    public List<Profile> loadAllDentists() {

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<User> dentistUsersCriteriaQuery = builder.createQuery(User.class);
            Root<User> dentistUsersRoot = dentistUsersCriteriaQuery.from(User.class);

            dentistUsersCriteriaQuery.select(dentistUsersRoot).where(builder.equal(dentistUsersRoot.get("role"), Role.Dentysta));
            List<User> dentistUsersList = session.createQuery(dentistUsersCriteriaQuery).getResultList();

            CriteriaQuery<Profile> dentistProfilesCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> dentistProfilesRoot = dentistProfilesCriteriaQuery.from(Profile.class);

            List<Profile> dentistProfilesList = new ArrayList<>();
            for(int i = 0; i < dentistUsersList.size(); i++){

                dentistProfilesCriteriaQuery.select(dentistProfilesRoot).where(builder.equal(dentistProfilesRoot.get("userGuid"), dentistUsersList.get(i)));
                Query sessionFindDentists = session.createQuery(dentistProfilesCriteriaQuery);
                dentistProfilesList.add((Profile) sessionFindDentists.getSingleResult());
            }

            return dentistProfilesList;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Adds Patient to database with given parameters and create its Card with default values
     *
     * @param firstName
     * @param lastName
     * @param PESEL
     * @param city
     * @param address
     * @param postalCode
     * @param email
     * @param phone
     * @param dentistProfile
     */
    public void addPatient(String firstName, String lastName, String PESEL, String city, String address, String postalCode, String email, String phone, String dentistProfile) {

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Profile profile = new Profile(firstName, lastName, PESEL, city, address, postalCode, email, phone, dentistProfile);
            session.save(profile);

            for (int i = 11; i < 49; i++) {
                if (i == 19 || i == 29 || i == 39) {
                    i++;
                } else {
                    ToothHistory toothHistory = new ToothHistory(i, new Date(System.currentTimeMillis()), ToothState.NONE, profile);
                    session.save(toothHistory);
                }
            }
            tx.commit();


        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;

        } finally {
            session.close();
        }
    }

    /**
     * Navigates to given FXML path using Paths enum.
     *
     * @param path Path enum to FXML file
     */
    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }

}
