package main.java.GUI.addPatient;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.regex.Pattern;

public class Controller {

    @FXML
    private TextField firstName;

    @FXML
    private TextField lastName;

    @FXML
    private TextField PESEL;

    @FXML
    private TextField city;

    @FXML
    private TextField address;

    @FXML
    private TextField postalCode;

    @FXML
    private ComboBox<String> defaultDentist;

    @FXML
    private TextField phone;

    @FXML
    private TextField email;

    @FXML
    private Button backBtn;

    @FXML
    private Button logoutBtn;

    @FXML
    private Button homeBtn;

    @FXML
    private Button confirmBtn;

    @FXML
    private Label messageLabel;

    Service service = new Service();

    /**
     * @param event
     */
    @FXML
    void backAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void homeAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    /**
     * Loading dentists from database and added them to list for Combobox
     */
    public void loadDentists(){
        List<Profile> dentistsList = service.loadAllDentists();
        for(int i = 0; i < dentistsList.size(); i++) {
            defaultDentist.getItems().add(dentistsList.get(i).getFirstName() + " " + dentistsList.get(i).getLastName());
        }
    }

    /**
     * Choose dentist from given names.
     *
     * @param names names of choosed dentist
     * @return
     */
    public String choosedDentist(String names){
        String[] dentistNames = names.split(" ");

        SessionFactory sessionFactory = Connector.getSessionFactory();
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();
        try{
            CriteriaBuilder builder = currentSession.getCriteriaBuilder();
            CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

            Predicate[] predicates = new Predicate[2];
            predicates[0] = builder.equal(profileRoot.get("firstName"), dentistNames[0]);
            predicates[1] = builder.equal(profileRoot.get("lastName"), dentistNames[1]);

            profileCriteriaQuery.select(profileRoot).where(predicates);
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            Profile foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();
            return foundProfile.getGUID();
        }
        catch(Exception e){
            throw e;
        }finally {
            currentSession.close();
        }
    }

    public void initialize() {
        loadDentists();
    }


    /**
     * Save patient to database from given values
     *
     * @param event
     */
    @FXML
    void confirmAction(ActionEvent event) {
        if (isInputValid()) {
            service.addPatient(firstName.getText(), lastName.getText(), PESEL.getText(), city.getText(), address.getText(), postalCode.getText(), email.getText(), phone.getText(), choosedDentist(defaultDentist.getValue()));

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Dodano pacjenta");
            alert.showAndWait();

            service.navigateToPath(Path.patientList);
        }
    }

    /**
     * Validation for patient data
     *
     * @return
     */
    private boolean isInputValid() {
        messageLabel.setText("");
        Pattern emailPattern = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)"
                + "*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")"
                + "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])"
                + "?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])"
                + "|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
        Pattern peselPattern = Pattern.compile("^\\d{11}$");
        Pattern phonePattern = Pattern.compile("(?<!\\w)(\\(?(\\+|00)?48\\)?)?[ -]?\\d{3}[ -]?\\d{3}[ -]?\\d{3}(?!\\w)");

        if(!(peselPattern.matcher(PESEL.getText()).matches())){
            messageLabel.setText("Nieprawidłowy numer PESEL");
            return false;
        }
        else if(!(phonePattern.matcher(phone.getText()).matches())){
            messageLabel.setText("Nieprawidłowy numer telefonu");
            return false;
        }
        else if(!(email.getText().trim().isEmpty())){
            if(!(emailPattern.matcher(email.getText()).matches())){
                messageLabel.setText("Nieprawidłowy adres e-mail");
                return false;
            }
        }
        return true;
    }


}
