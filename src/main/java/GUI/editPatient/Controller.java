package main.java.GUI.editPatient;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.enums.Role;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class Controller implements Initializable{

    @FXML
    private TextField firstNameField;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField peselField;

    @FXML
    private TextField cityField;

    @FXML
    private TextField addressField;

    @FXML
    private TextField postalCodeField;

    @FXML
    private TextField phoneField;

    @FXML
    private TextField emailField;

    @FXML
    private ComboBox<String> defaultDentist;

    @FXML
    private Button backBtn;

    @FXML
    private Button logoutBtn;

    @FXML
    private Button homeBtn;

    @FXML
    private Button confirmBtn;

    @FXML
    private Label messageLabel;

    Service service = new Service();

    @FXML
    void backAction(ActionEvent event) {
        service.navigateToPath(Path.patientList);
    }

    @FXML
    void homeAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    /**
     * Load all dentists from database
     */
    public void loadDentists(){
        List<Profile> dentistsList = service.loadAllDentists();
        for(int i = 0; i < dentistsList.size(); i++) {
            defaultDentist.getItems().add(dentistsList.get(i).getFirstName() + " " + dentistsList.get(i).getLastName());
        }
    }

    /**
     * Choose dentist from given names
     *
     * @param names dentist's first name and last name
     * @return dentist id
     */
    public String choosedDentist(String names){
        String[] dentistNames = names.split(" ");

        SessionFactory sessionFactory = Connector.getSessionFactory();
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();
        try{
            CriteriaBuilder builder = currentSession.getCriteriaBuilder();
            CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

            Predicate[] predicates = new Predicate[2];
            predicates[0] = builder.equal(profileRoot.get("firstName"), dentistNames[0]);
            predicates[1] = builder.equal(profileRoot.get("lastName"), dentistNames[1]);

            profileCriteriaQuery.select(profileRoot).where(predicates);
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            Profile foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();
            return foundProfile.getGUID();
        }
        catch(Exception e){
            throw e;
        }finally {
            currentSession.close();
        }
    }

    @FXML
    void confirmAction(ActionEvent event) {
        if (isInputValid()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Czy jesteś pewny/a, że chcesz nadpisać"
                    + " dane tego pacjenta?", ButtonType.YES, ButtonType.CANCEL);
            alert.setHeaderText("Edytowanie pacjenta");
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                updatePatient();
                service.navigateToPath(Path.patientList);
            }
        }
    }

    /**
     * Validation for patient data
     *
     * @return
     */
    private boolean isInputValid() {
        messageLabel.setText("");
        Pattern emailPattern = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)"
                + "*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")"
                + "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])"
                + "?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])"
                + "|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
        Pattern peselPattern = Pattern.compile("^\\d{11}$");
        Pattern phonePattern = Pattern.compile("(?<!\\w)(\\(?(\\+|00)?48\\)?)?[ -]?\\d{3}[ -]?\\d{3}[ -]?\\d{3}(?!\\w)");

        if(!(peselPattern.matcher(peselField.getText()).matches())){
            messageLabel.setText("Nieprawidłowy numer PESEL");
            return false;
        }
        else if(!(phonePattern.matcher(phoneField.getText()).matches())){
            messageLabel.setText("Nieprawidłowy numer telefonu");
            return false;
        }
        else if(!(emailField.getText().trim().isEmpty())){
            if(!(emailPattern.matcher(emailField.getText()).matches())){
                messageLabel.setText("Nieprawidłowy adres e-mail");
                return false;
            }
        }
        return true;
    }

    /**
     * Shows patient data
     */
    public void setPatientDetails(){
        Profile editFromPatientsList = AppSession.getInstance().getEditFromPatientsList();
        firstNameField.setText(editFromPatientsList.getFirstName());
        lastNameField.setText(editFromPatientsList.getLastName());
        peselField.setText(editFromPatientsList.getPESEL());
        cityField.setText(editFromPatientsList.getCity());
        addressField.setText(editFromPatientsList.getAddress());
        postalCodeField.setText(editFromPatientsList.getPostalCode());
        phoneField.setText(editFromPatientsList.getPhone());
        emailField.setText(editFromPatientsList.getEmail());
        defaultDentist.setValue(service.names(service.getProfileByGUID(editFromPatientsList.getDefaultDentistGuid())));

    }

    /**
     * Updates patient data from given values
     */
public void updatePatient(){
        service.updatePatient(firstNameField.getText(), lastNameField.getText(), peselField.getText(),
                cityField.getText(), addressField.getText(), postalCodeField.getText(), phoneField.getText(), emailField.getText(), choosedDentist(defaultDentist.getValue()));
    }



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadDentists();
        setPatientDetails();
    }

}
