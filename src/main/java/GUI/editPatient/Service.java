package main.java.GUI.editPatient;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.User;
import main.java.database.enums.Role;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class Service {

    /**
     * Loads list of all dentists in database
     *
     * @return list of dentists
     */
    public List<Profile> loadAllDentists() {

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<User> dentistUsersCriteriaQuery = builder.createQuery(User.class);
            Root<User> dentistUsersRoot = dentistUsersCriteriaQuery.from(User.class);

            dentistUsersCriteriaQuery.select(dentistUsersRoot).where(builder.equal(dentistUsersRoot.get("role"), Role.Dentysta));
            List<User> dentistUsersList = session.createQuery(dentistUsersCriteriaQuery).getResultList();

            CriteriaQuery<Profile> dentistProfilesCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> dentistProfilesRoot = dentistProfilesCriteriaQuery.from(Profile.class);

            List<Profile> dentistProfilesList = new ArrayList<>();
            for(int i = 0; i < dentistUsersList.size(); i++){

                dentistProfilesCriteriaQuery.select(dentistProfilesRoot).where(builder.equal(dentistProfilesRoot.get("userGuid"), dentistUsersList.get(i)));
                Query sessionFindDentists = session.createQuery(dentistProfilesCriteriaQuery);
                dentistProfilesList.add((Profile) sessionFindDentists.getSingleResult());
            }

            return dentistProfilesList;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Updates patient data from given values
     *
     * @param firstName
     * @param lastName
     * @param PESEL
     * @param city
     * @param address
     * @param postalCode
     * @param phone
     * @param email
     * @param defaultDentist
     */
    public void updatePatient(String firstName, String lastName, String PESEL, String city,
                           String address, String postalCode, String phone, String email, String defaultDentist){
        Session session = Connector.openSession();
        Transaction tx = null;
        Profile editFromPatientsList = AppSession.getInstance().getEditFromPatientsList();
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);

            userProfileCriteriaQuery.select(userProfileRoot).where(builder.equal(userProfileRoot.get("PESEL"), editFromPatientsList.getPESEL()));
            Profile userProfile = (Profile) session.createQuery(userProfileCriteriaQuery).getSingleResult();

            userProfile.setFirstName(firstName);
            userProfile.setLastName(lastName);
            userProfile.setPESEL(PESEL);
            userProfile.setCity(city);
            userProfile.setAddress(address);
            userProfile.setPostalCode(postalCode);
            userProfile.setPhone(phone);
            userProfile.setEmail(email);
            userProfile.setDefaultDentistGuid(defaultDentist);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Navigates to FXML file from given path
     *
     * @param path
     */
    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }

    /**
     * Find Profile in database by its ID
     *
     * @param GUID
     * @return Profile
     */
    public Profile getProfileByGUID(String GUID) {
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("GUID"), GUID));

            return session.createQuery(profileCriteriaQuery).getSingleResult();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        }
    }

    /**
     * Joins first name and last name from profile
     *
     * @param profile
     * @return
     */
    public String names(Profile profile){
        return profile.getFirstName() + " " + profile.getLastName();
    }
}
