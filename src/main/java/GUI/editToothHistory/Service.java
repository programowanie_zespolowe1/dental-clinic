package main.java.GUI.editToothHistory;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.ToothHistory;
import main.java.database.entity.User;
import main.java.database.enums.ToothState;
import main.java.entry.AppSession;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Date;

public class Service {

    public void navigateToPath(main.java.navigator.Path path){
        main.java.entry.AppSession appSession = main.java.entry.AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }

    public void updateTootHistory(Date visitDate, String history, String xRay, ToothState toothState){
        Session session = Connector.openSession();
        Transaction tx = null;
        ToothHistory toothHistoryForDetails = AppSession.getInstance().getEditToToothHistory();
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<ToothHistory> toothHistoryCriteriaQuery = builder.createQuery(ToothHistory.class);
            Root<ToothHistory> toothHistoryRoot = toothHistoryCriteriaQuery.from(ToothHistory.class);
            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(toothHistoryRoot.get("toothNumber"), toothHistoryForDetails.getToothNumber());
            predicates[1] = builder.equal(toothHistoryRoot.get("visitDate"), toothHistoryForDetails.getVisitDate());
            predicates[2] = builder.equal(toothHistoryRoot.get("history"), toothHistoryForDetails.getHistory());
            predicates[3] = builder.equal(toothHistoryRoot.get("profile"), toothHistoryForDetails.getProfile());

            toothHistoryCriteriaQuery.select(toothHistoryRoot).where(predicates);
            ToothHistory foundToothHistory = (ToothHistory) session.createQuery(toothHistoryCriteriaQuery).getSingleResult();

            foundToothHistory.setVisitDate(visitDate);
            foundToothHistory.setHistory(history);
            foundToothHistory.setxRayPicture(xRay);
            foundToothHistory.setToothState(toothState);

            session.getTransaction().commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}
