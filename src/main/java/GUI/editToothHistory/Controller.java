package main.java.GUI.editToothHistory;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.scene.text.Text;
import main.java.database.entity.Profile;
import main.java.database.entity.ToothHistory;
import main.java.database.enums.Role;
import main.java.database.enums.ToothState;
import main.java.entry.AppSession;
import main.java.navigator.Path;

import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class Controller implements Initializable {

    @FXML
    private Text toothNumberField;

    @FXML
    private DatePicker calendarField;


    @FXML
    private ChoiceBox<String> toothStateField;

    @FXML
    private TextField xRayPictureField;

    @FXML
    private TextArea historyField;

    @FXML
    private Button confirmBtn;

    @FXML
    private Button backBtn;

    @FXML
    private Button logoutBtn;

    @FXML
    private Button homeBtn;

    private Service service = new Service();

    @FXML
    void backAction(ActionEvent event) {
        main.java.entry.AppSession appSession = main.java.entry.AppSession.getInstance();

        appSession.getNavigator().navigateTo(main.java.navigator.Path.patientCard);
    }

    @FXML
    void confirmAction(ActionEvent event) {
        updateToothHistory();
        service.navigateToPath(Path.patientCard);

    }

    @FXML
    void homeAction(ActionEvent event) {
        main.java.entry.AppSession appSession = main.java.entry.AppSession.getInstance();

        appSession.getNavigator().navigateTo(main.java.navigator.Path.landing);
    }



    @FXML
    void logoutAction(ActionEvent event) {
        AppSession appSession = AppSession.getInstance();

        appSession.getNavigator().navigateTo(Path.login);
    }

    public void setToothHistoryDetails(){
        ToothHistory editToToothHistory = AppSession.getInstance().getEditToToothHistory();
        toothNumberField.setText(String.valueOf(editToToothHistory.getToothNumber()));
        calendarField.setValue(editToToothHistory.getVisitDate().toLocalDate());
        toothStateField.setValue(String.valueOf(ToothState.valueOf(String.valueOf(editToToothHistory.getToothState()))));
        xRayPictureField.setText(editToToothHistory.getxRayPicture());
        historyField.setText(editToToothHistory.getHistory());
    }

    public void initialize(URL url, ResourceBundle resourceBundle) {

        setToothHistoryDetails();
    }

    public void updateToothHistory(){
        service.updateTootHistory(Date.valueOf(calendarField.getValue()), historyField.getText(),xRayPictureField.getText(), ToothState.valueOf(toothStateField.getValue()));
    }
}
