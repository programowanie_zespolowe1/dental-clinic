package main.java.GUI.adminAddUser;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.User;
import main.java.database.enums.Role;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Service {

    /**
     * Adds a User to database and its Profile with given parameters.
     *
     * @param firstName     user first name
     * @param lastName      user last name
     * @param PESEL         user PESEL
     * @param city          user city
     * @param address       user address line 1
     * @param postalCode    user postal code
     * @param email         user email
     * @param phone         user phone
     * @param userName      user login
     * @param password      user password
     * @param role          user role
     */
    public static void addUser(String firstName, String lastName, String PESEL, String city, String address,
                               String postalCode, String email, String phone, String userName, String password, Role role){

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Profile profile = new Profile(firstName, lastName, PESEL, city, address, postalCode, email, phone);
            session.save(profile);
            User user = new User(userName, password, role, profile);
            session.save(user);
            profile.setUserGuid(user);
            tx.commit();
        }
        catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        }
        finally {
            session.close();
        }

    }

    /**
     *  Navigates to given FXML path using Paths enum.
     *
     *  @param path Path enum to FXML file
     */
    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }


}
