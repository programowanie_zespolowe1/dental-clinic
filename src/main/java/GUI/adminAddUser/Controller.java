package main.java.GUI.adminAddUser;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import main.java.database.enums.Role;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import java.util.regex.Pattern;

public class Controller {

    @FXML
    private TextField nameField;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField peselField;

    @FXML
    private TextField cityField;

    @FXML
    private TextField postalCodeField;

    @FXML
    private TextField phoneField;

    @FXML
    private TextField streetField;

    @FXML
    private TextField emailField;

    @FXML
    private ChoiceBox<String> roleField;

    @FXML
    private TextField loginField;

    @FXML
    private TextField passwordField;


    private Service service = new Service();

    @FXML
    private Button confirmBtn;

    @FXML
    private Button backBtn;

    @FXML
    private Button logoutBtn;

    @FXML
    private Button homeBtn;

    @FXML
    private Label messageLabel;


    @FXML
    void backAction(ActionEvent event) {
        service.navigateToPath(Path.userList);
    }

    @FXML
    void confirmAction(ActionEvent event) {

        if (isInputValid()) {
            service.addUser(nameField.getText(), lastNameField.getText(), peselField.getText(),
                    cityField.getText(), streetField.getText(), postalCodeField.getText(), emailField.getText(),
                    phoneField.getText(), loginField.getText(), passwordField.getText(), Role.valueOf(roleField.getValue()));

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Dodano użytkownika");
            alert.showAndWait();

            service.navigateToPath(Path.userList);
        }
    }

    @FXML
    void homeAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    private boolean isInputValid() {
        messageLabel.setText("");
        Pattern emailPattern = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)"
                + "*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")"
                + "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])"
                + "?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])"
                + "|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
        Pattern peselPattern = Pattern.compile("^\\d{11}$");
        Pattern phonePattern = Pattern.compile("(?<!\\w)(\\(?(\\+|00)?48\\)?)?[ -]?\\d{3}[ -]?\\d{3}[ -]?\\d{3}(?!\\w)");
        Pattern loginPattern = Pattern.compile("^\\w{3,}$");
        Pattern passwordPattern = Pattern.compile("^\\w{6,}$");

        if(!(peselPattern.matcher(peselField.getText()).matches())){
            messageLabel.setText("Nieprawidłowy numer PESEL");
            return false;
        }
        else if(!(phonePattern.matcher(phoneField.getText()).matches())){
            messageLabel.setText("Nieprawidłowy numer telefonu");
            return false;
        }
        else if(!(emailPattern.matcher(emailField.getText()).matches())){
            messageLabel.setText("Nieprawidłowy adres e-mail");
            return false;
        }
        else if(!(loginPattern.matcher(loginField.getText()).matches())){
            messageLabel.setText("Login musi zawierać min. 3 znaki");
            return false;
        }
        else if(!(passwordPattern.matcher(passwordField.getText()).matches())){
            messageLabel.setText("Hasło musi zawierać min. 6 znaków");
            return false;
        }
        return true;
    }
}
