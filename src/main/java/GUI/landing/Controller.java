package main.java.GUI.landing;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import main.java.database.enums.Role;
import main.java.entry.AppSession;
import main.java.navigator.Path;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {


    @FXML
    private Button logoutBtn;

    @FXML
    private Button calendarBtn;

    @FXML
    private Button patientListBtn;

    @FXML
    private Button userListBtn;

    @FXML
    private Button addUserBtn;

    @FXML
    private Button addPatientBtn;

    @FXML
    private Button userInfoBtn;

    Service service = new Service();

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    @FXML
    void showCalendarAction(ActionEvent event) {
        service.navigateToPath(Path.calendar);

    }

    @FXML
    void showPatientListAction(ActionEvent event) {
        service.navigateToPath(Path.patientList);
    }

    @FXML
    void addPatientAction(ActionEvent event) {
        service.navigateToPath(Path.addPatient);
    }

    @FXML
    void addUserAction(ActionEvent event) {
        service.navigateToPath(Path.addUser);
    }

    @FXML
    void showUserInfoAction(ActionEvent event) {
       service.navigateToPath(Path.userInfo);

    }

    @FXML
    void userListAction(ActionEvent event) {
        service.navigateToPath(Path.userList);
    }

    /**
     * Sets view of buttons depends on who is logged in
     */
    void setButtonsView(){
        Role userRole = AppSession.getInstance().getLoggedUser().getRole();
        if(userRole==Role.Dentysta || userRole==Role.Higienistka){
            addPatientBtn.setVisible(false);
            addUserBtn.setVisible(false);
            userListBtn.setVisible(false);
            userInfoBtn.setLayoutY(169.0);
        }
        if(userRole==Role.Recepcjonista){
            addUserBtn.setVisible(false);
            userListBtn.setVisible(false);
        }
    }

    public void initialize(URL url, ResourceBundle resourceBundle) {
        setButtonsView();
    }


}