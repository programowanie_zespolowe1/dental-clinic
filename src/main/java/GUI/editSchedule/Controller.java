package main.java.GUI.editSchedule;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import main.java.database.entity.Profile;
import main.java.database.entity.Schedule;
import main.java.entry.AppSession;
import main.java.navigator.Path;

import java.net.URL;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;


public class Controller implements Initializable {

    @FXML
    private ComboBox<String> mondayFromBox;

    @FXML
    private ComboBox<String> mondayToBox;

    @FXML
    private ComboBox<String> tuesdayFromBox;

    @FXML
    private ComboBox<String> tuesdayToBox;

    @FXML
    private ComboBox<String> wednesdayFromBox;

    @FXML
    private ComboBox<String> wednesdayToBox;

    @FXML
    private ComboBox<String> thursdayFromBox;

    @FXML
    private ComboBox<String> thursdayToBox;

    @FXML
    private ComboBox<String> fridayFromBox;

    @FXML
    private ComboBox<String> fridayToBox;

    @FXML
    private ComboBox<String> saturdayFromBox;

    @FXML
    private ComboBox<String> saturdayToBox;

    @FXML
    private ComboBox<String> sundayFromBox;

    @FXML
    private ComboBox<String> sundayToBox;

    List<Schedule> scheduleList;

    private Service service = new Service();

    @FXML
    void backAction(ActionEvent event) {
        service.navigateToPath(Path.userInfo);
    }

    /**
     * Checks if both start hour and end hour for day of week is not null,
     * if so sends them to Service.
     *
     * @param event
     */
    @FXML
    void confirmAction(ActionEvent event) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Czy jesteś pewny/a, że chcesz nadpisać"
                + " dane?", ButtonType.YES, ButtonType.CANCEL);
        alert.setHeaderText("Edytowanie grafiku");
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            Profile userProfile = AppSession.getInstance().getLoggedUserProfile();

            scheduleList.clear();

            if(mondayFromBox.getValue() != null  && mondayToBox.getValue() != null){
                scheduleList.add(new Schedule(Time.valueOf(mondayFromBox.getValue()),
                        Time.valueOf(mondayToBox.getValue()), "MONDAY", userProfile));
            }
            if(tuesdayFromBox.getValue() != null  && tuesdayToBox.getValue() != null){
                scheduleList.add(new Schedule(Time.valueOf(tuesdayFromBox.getValue()),
                        Time.valueOf(tuesdayToBox.getValue()), "TUESDAY", userProfile));
            }
            if(wednesdayFromBox.getValue() != null  && wednesdayToBox.getValue() != null){
                scheduleList.add(new Schedule(Time.valueOf(wednesdayFromBox.getValue()),
                        Time.valueOf(wednesdayToBox.getValue()), "WEDNESDAY", userProfile));
            }
            if(thursdayFromBox.getValue() != null  && thursdayToBox.getValue() != null){
                scheduleList.add(new Schedule(Time.valueOf(thursdayFromBox.getValue()),
                        Time.valueOf(thursdayToBox.getValue()), "THURSDAY", userProfile));
            }
            if(fridayFromBox.getValue() != null  && fridayToBox.getValue() != null){
                scheduleList.add(new Schedule(Time.valueOf(fridayFromBox.getValue()),
                        Time.valueOf(fridayToBox.getValue()), "FRIDAY", userProfile));
            }
            if(saturdayFromBox.getValue() != null  && saturdayToBox.getValue() != null){
                scheduleList.add(new Schedule(Time.valueOf(saturdayFromBox.getValue()),
                        Time.valueOf(saturdayToBox.getValue()), "SATURDAY", userProfile));
            }
            if(sundayFromBox.getValue() != null  && sundayToBox.getValue() != null){
                scheduleList.add(new Schedule(Time.valueOf(sundayFromBox.getValue()),
                        Time.valueOf(sundayToBox.getValue()), "SUNDAY", userProfile));
            }
            service.updateScheduleForUser(scheduleList, userProfile);
            service.navigateToPath(Path.userInfo);

        }
    }

    @FXML
    void homeAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    /**
     * Gets list of Schedules for Profile (saved in AppSession) and displays start hours and end hours
     * for the particular day of the week.
     */
    public void displayUserSchedule() {

        Profile userProfile = AppSession.getInstance().getLoggedUserProfile();
        scheduleList = service.getScheduleForUser(userProfile);

        Map<String, List<String>> scheduleDay = new HashMap<>();

        if (scheduleList != null) {
            for (int i = 0; i < scheduleList.size(); i++) {

                List<String> timeForDay = new ArrayList<>();

                String dayOfWeek = scheduleList.get(i).getDayOfWeek();

                timeForDay.add(String.valueOf(scheduleList.get(i).getStartTime()));
                timeForDay.add(String.valueOf(scheduleList.get(i).getEndTime()));
                scheduleDay.put(dayOfWeek, timeForDay);
            }
        }

        if(scheduleDay.containsKey("MONDAY")) {
            String mondayTime1 = scheduleDay.get("MONDAY").get(0);
            String mondayTime2 = scheduleDay.get("MONDAY").get(1);

            mondayFromBox.setValue(mondayTime1);
            mondayToBox.setValue(mondayTime2);
        }
        if(scheduleDay.containsKey("TUESDAY")) {
            String tuesdayTime1 = scheduleDay.get("TUESDAY").get(0);
            String tuesdayTime2 = scheduleDay.get("TUESDAY").get(1);

            tuesdayFromBox.setValue(tuesdayTime1);
            tuesdayToBox.setValue(tuesdayTime2);
        }
        if(scheduleDay.containsKey("WEDNESDAY")) {
            String wednesdayTime1 = scheduleDay.get("WEDNESDAY").get(0);
            String wednesdayTime2 = scheduleDay.get("WEDNESDAY").get(1);

            wednesdayFromBox.setValue(wednesdayTime1);
            wednesdayToBox.setValue(wednesdayTime2);
        }
        if(scheduleDay.containsKey("THURSDAY")) {
            String thursdayTime1 = scheduleDay.get("THURSDAY").get(0);
            String thursdayTime2 = scheduleDay.get("THURSDAY").get(1);

            thursdayFromBox.setValue(thursdayTime1);
            thursdayToBox.setValue(thursdayTime2);
        }
        if(scheduleDay.containsKey("FRIDAY")) {
            String fridayTime1 = scheduleDay.get("FRIDAY").get(0);
            String fridayTime2 = scheduleDay.get("FRIDAY").get(1);

            fridayFromBox.setValue(fridayTime1);
            fridayToBox.setValue(fridayTime2);
        }
        if(scheduleDay.containsKey("SATURDAY")) {
            String saturdayTime1 = scheduleDay.get("SATURDAY").get(0);
            String saturdayTime2 = scheduleDay.get("SATURDAY").get(1);

            saturdayFromBox.setValue(saturdayTime1);
            saturdayToBox.setValue(saturdayTime2);
        }
        if(scheduleDay.containsKey("SUNDAY")) {
            String sundayTime1 = scheduleDay.get("SUNDAY").get(0);
            String sundayTime2 = scheduleDay.get("SUNDAY").get(1);

            sundayFromBox.setValue(sundayTime1);
            sundayToBox.setValue(sundayTime2);
        }
    }

    /**
     * Populates ChoiceBoxes with hours from 8:00 to 21:00 with half hour intervals.
     */
    public void populateChoiceBox(){

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        List<String> dayTimeList = FXCollections.observableArrayList();

        Time startTime = Time.valueOf("08:00:00");
        Time endTime = Time.valueOf("21:30:00");

        Calendar cal = Calendar.getInstance();
        cal.setTime(startTime);

        dayTimeList.add(null);

        while(cal.getTime().before(endTime)){

            dayTimeList.add(sdf.format(cal.getTime()));

            cal.add(Calendar.MINUTE, 30);

        }
        mondayFromBox.setItems((ObservableList<String>) dayTimeList);
        mondayToBox.setItems((ObservableList<String>) dayTimeList);
        tuesdayFromBox.setItems((ObservableList<String>) dayTimeList);
        tuesdayToBox.setItems((ObservableList<String>) dayTimeList);
        wednesdayFromBox.setItems((ObservableList<String>) dayTimeList);
        wednesdayToBox.setItems((ObservableList<String>) dayTimeList);
        thursdayFromBox.setItems((ObservableList<String>) dayTimeList);
        thursdayToBox.setItems((ObservableList<String>) dayTimeList);
        fridayFromBox.setItems((ObservableList<String>) dayTimeList);
        fridayToBox.setItems((ObservableList<String>) dayTimeList);
        saturdayFromBox.setItems((ObservableList<String>) dayTimeList);
        saturdayToBox.setItems((ObservableList<String>) dayTimeList);
        sundayFromBox.setItems((ObservableList<String>) dayTimeList);
        sundayToBox.setItems((ObservableList<String>) dayTimeList);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        populateChoiceBox();

        displayUserSchedule();
    }
}
