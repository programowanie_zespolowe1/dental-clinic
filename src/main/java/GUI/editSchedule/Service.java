package main.java.GUI.editSchedule;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.Schedule;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class Service {

    /**
     * Gets all Schedules for given Profile.
     *
     * @param profile Profile
     * @return list of schedules
     */
    public List<Schedule> getScheduleForUser(Profile profile) {
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Schedule> scheduleCriteriaQuery = builder.createQuery(Schedule.class);
            Root<Schedule> scheduleRoot = scheduleCriteriaQuery.from(Schedule.class);

            scheduleCriteriaQuery.select(scheduleRoot).where(builder.equal(scheduleRoot.get("profile"), profile));
            List<Schedule> scheduleList = session.createQuery(scheduleCriteriaQuery).getResultList();

            return scheduleList;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Searches database for all Schedules for given Profile, deletes them and creates new ones
     * from given list.
     *
     * @param listOfSchedule    List list of new schedules
     * @param profile           Profile
     */
    public void updateScheduleForUser(List<Schedule> listOfSchedule, Profile profile) {
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Schedule> scheduleCriteriaQuery = builder.createQuery(Schedule.class);
            Root<Schedule> scheduleRoot = scheduleCriteriaQuery.from(Schedule.class);

            scheduleCriteriaQuery.select(scheduleRoot).where(builder.equal(scheduleRoot.get("profile"), profile));
            List<Schedule> scheduleList = session.createQuery(scheduleCriteriaQuery).getResultList();

            clearSchedule(profile);

            for(int i = 0; i<listOfSchedule.size(); i++){
                scheduleList.add(listOfSchedule.get(i));
            }

            for(int i = 0; i < scheduleList.size(); i++){
                session.save(scheduleList.get(i));
            }

            session.getTransaction().commit();

        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Searches database for all Schedules for given Profile and deletes them.
     *
     * @param profile   Profile user profile
     */
    public void clearSchedule(Profile profile) {
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Schedule> scheduleCriteriaQuery = builder.createQuery(Schedule.class);
            Root<Schedule> scheduleRoot = scheduleCriteriaQuery.from(Schedule.class);

            scheduleCriteriaQuery.select(scheduleRoot).where(builder.equal(scheduleRoot.get("profile"), profile));
            List<Schedule> scheduleList = session.createQuery(scheduleCriteriaQuery).getResultList();

            for(int i = 0; i < scheduleList.size(); i++){
                session.delete(scheduleList.get(i));
            }

            session.getTransaction().commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     *  Navigates to given FXML path using Paths enum.
     *
     *  @param path Path enum to FXML file
     */
    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }
}
