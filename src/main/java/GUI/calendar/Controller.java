package main.java.GUI.calendar;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import main.java.database.entity.Profile;
import main.java.entry.AppSession;
import main.java.navigator.Path;

public class Controller implements Initializable {

    @FXML
    private Button appointmentBtn;

    @FXML
    private Button backBtn;

    @FXML
    private Button logoutBtn;

    @FXML
    private Button yearUpButton;

    @FXML
    private Button yearDownButton;

    @FXML
    private Button monthDownButton;

    @FXML
    private Button monthUpButton;

    @FXML
    private Button homeBtn;

    @FXML
    private GridPane calendar;

    @FXML
    private TextField yearField;

    @FXML
    private TextField monthField;

    @FXML
    private Button confirmButton;

    @FXML
    private ComboBox<String> dentistsBox;


    private LocalDate localDate;

    public String date;

    Service service = new Service();

    /**
     * Sets current date and displays calendar for current month and year.
     *
     * @param url
     * @param resourceBundle
     */
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadDentists();
        localDate = LocalDate.now();
        setDateShow(localDate);
        monthField.setText(String.valueOf(localDate.getMonthValue()));
        yearField.setText(String.valueOf(localDate.getYear()));

    }

    @FXML
    void backAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void homeAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    public void loadDentists(){
        List<Profile> dentistsList = service.loadAllDentists();
        for(int i = 0; i < dentistsList.size(); i++) {
            dentistsBox.getItems().add(dentistsList.get(i).getFirstName() + " " + dentistsList.get(i).getLastName());
        }
    }

    /**
     * Decreases number of the month in textfield by one (not less than 1).
     *
     * @param event
     */
    @FXML
    void monthDownAction(ActionEvent event) {

        try {
            int monthValue = Integer.parseInt(monthField.getText());

            if (monthValue > 1) {

                monthValue--;

                String x = Integer.toString(monthValue);

                monthField.setText(x);
            }
        }catch(NumberFormatException e) {
            e.printStackTrace();
        }

    }

    /**
     * Increases number of the month in texfield by one(not higher than 12).
     *
     * @param event
     */
    @FXML
    void monthUpAction(ActionEvent event) {

        try{
            int monthValue = Integer.parseInt(monthField.getText());

            if (monthValue < 12) {

                monthValue++;

                String x = Integer.toString(monthValue);

                monthField.setText(x);
            }
        }catch (NumberFormatException e) {
            e.printStackTrace();
        }


    }

    /**
     * Decreases number of the year in textfield by one.
     *
     * @param event
     */
    @FXML
    void yearDownAction(ActionEvent event) {

        try {
            int yearValue = Integer.parseInt(yearField.getText());

            yearValue--;

            String x = Integer.toString(yearValue);

            yearField.setText(x);
        }catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }


    /**
     * Increases number of the year in textfield by one.
     *
     * @param event
     */
    @FXML
    void yearUpAction(ActionEvent event) {

        try{
            int yearValue = Integer.parseInt(yearField.getText());

            yearValue++;

            String x = Integer.toString(yearValue);

            yearField.setText(x);
        }catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets date from textfields and displays calendar for given month and year.
     * @param event
     */
    @FXML
    void confirmAction(ActionEvent event) {
        int year = Integer.valueOf(yearField.getText());
        int month = Integer.valueOf(monthField.getText());
        LocalDate date = LocalDate.of(year, month, 1);
        setDateShow(date);
    }

    /**
     * Displays month days as buttons with action navigateToVisitList.
     * From first day of the month pulls day of the week and the following days.
     *
     * @param localDate LocalDate
     *
     */
    public void setDateShow(LocalDate localDate) {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy MM");

        LocalDate firstDay = localDate.withDayOfMonth(1);
        int dateIndex = firstDay.getDayOfWeek().getValue();

        int daysOfMonth = getMonthDays(localDate.getYear(), localDate.getMonthValue());

        int days = 1;

        calendar.getChildren().clear();

        for (int column = dateIndex-1; column <= 6; column++) {
            Button button = new Button(String.valueOf(days));
            button.setPrefSize(200, 200);
            button.setOnAction(e -> navigateToVisitList(button.getText()));
            calendar.add(button, column, 0);
            days++;
            daysOfMonth--;
        }

        for (int row = 1; row <= 6; row++) {
            for (int column = 0; column <= 6; column++) {
                Button button = new Button(String.valueOf(days));
                button.setPrefSize(200, 200);
                button.setOnAction(e -> navigateToVisitList(button.getText()));
                calendar.add(button, column, row);
                days++;
                daysOfMonth--;

                if (0 == daysOfMonth) {
                    break;
                }
            }

            if (0 == daysOfMonth) {
                break;
            }
        }

    }

    /**
     * Calculates number of days for given month and year.
     *
     * @param year year as number
     * @param month month as number
     * @return number of days in the month
     */
    public int getMonthDays(int year, int month) {

        int monthDays = 0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                monthDays = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                monthDays = 30;
                break;
            case 2:
                if ((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0)) {
                    monthDays = 29;
                } else {
                    monthDays = 28;
                }
                break;
        }
        return monthDays;
    }


    public void navigateToVisitList(String dayButton) {

        if (!(dentistsBox.getValue().isEmpty())) {
            AppSession.getInstance().setDentistForCalendar(service.loadAllDentists().get(dentistsBox.getSelectionModel().getSelectedIndex()));

            AppSession.getInstance().setAppointmentDate(dayButton, monthField.getText(), yearField.getText());

            AppSession.getInstance().getNavigator().navigateTo(Path.appointmentList);
        }
    }
}
