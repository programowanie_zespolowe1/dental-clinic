package main.java.GUI.adminUserDetails;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.User;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class Service {

    /**
     * Gets User from database with given Profile.
     *
     * @param profile   selected Profile
     * @return found User
     */
    public User getUser(Profile profile){
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);
            CriteriaQuery<User> userCriteriaQuery = builder.createQuery(User.class);
            Root<User> userRoot = userCriteriaQuery.from(User.class);

            userProfileCriteriaQuery.select(userProfileRoot).where(builder.equal(userProfileRoot.get("PESEL"), profile.getPESEL()));
            Profile userProfile = (Profile) session.createQuery(userProfileCriteriaQuery).getSingleResult();

            userCriteriaQuery.select(userRoot).where(builder.equal(userRoot.get("profile"), userProfile));
            User user = (User) session.createQuery(userCriteriaQuery).getSingleResult();

            return user;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Searches database for Profile with given PESEL and its User and deletes them.
     *
     * @param pesel Profile PESEL
     */
    public void deleteUser(String pesel){

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);
            CriteriaQuery<User> userCriteriaQuery = builder.createQuery(User.class);
            Root<User> userRoot = userCriteriaQuery.from(User.class);

            userProfileCriteriaQuery.select(userProfileRoot).where(builder.equal(userProfileRoot.get("PESEL"), pesel));
            Profile userProfile = (Profile) session.createQuery(userProfileCriteriaQuery).getSingleResult();

            userCriteriaQuery.select(userRoot).where(builder.equal(userRoot.get("profile"), userProfile));
            User user = (User) session.createQuery(userCriteriaQuery).getSingleResult();

            session.delete(user);
            session.delete(userProfile);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Searches database for User (saved in AppSession) and its Profile
     * and updates them with given parameters.
     *
     * @param firstName     Profile  first name
     * @param lastName      Profile  last name
     * @param PESEL         Profile  PESEL
     * @param city          Profile  city
     * @param address       Profile  address line 1
     * @param postalcode    Profile  postal code
     * @param email         Profile  email
     * @param phone         Profile  phone
     * @param userName      User     username
     */
    public void updateUser(String firstName, String lastName, String PESEL, String phone, String email, String city,
                           String address, String postalcode, String userName){
        Session session = Connector.openSession();
        Transaction tx = null;
        Profile userForDetails = AppSession.getInstance().getUserForDetails();
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> userProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> userProfileRoot = userProfileCriteriaQuery.from(Profile.class);
            CriteriaQuery<User> userCriteriaQuery = builder.createQuery(User.class);
            Root<User> userRoot = userCriteriaQuery.from(User.class);

            userProfileCriteriaQuery.select(userProfileRoot).where(builder.equal(userProfileRoot.get("PESEL"), userForDetails.getPESEL()));
            Profile userProfile = (Profile) session.createQuery(userProfileCriteriaQuery).getSingleResult();

            userCriteriaQuery.select(userRoot).where(builder.equal(userRoot.get("profile"), userProfile));
            User user = (User) session.createQuery(userCriteriaQuery).getSingleResult();

            user.setUserName(userName);
            userProfile.setFirstName(firstName);
            userProfile.setLastName(lastName);
            userProfile.setPESEL(PESEL);
            userProfile.setPhone(phone);
            userProfile.setEmail(email);
            userProfile.setCity(city);
            userProfile.setAddress(address);
            userProfile.setPostalCode(postalcode);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     *  Navigates to given FXML path using Paths enum.
     *
     *  @param path Path enum to FXML file
     */
    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }
}
