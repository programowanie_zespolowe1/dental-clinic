package main.java.GUI.adminUserDetails;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import main.java.database.entity.Profile;
import main.java.database.enums.Role;
import main.java.entry.AppSession;
import main.java.navigator.Path;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class Controller implements Initializable {

    @FXML
    private Button editUserBtn;

    @FXML
    private Button deleteUserBtn;

    @FXML
    private ComboBox<Role> roleBox;

    @FXML
    private TextField nameField;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField peselField;

    @FXML
    private TextField phoneField;

    @FXML
    private TextField cityField;

    @FXML
    private TextField streetField;

    @FXML
    private TextField postalcodeField;

    @FXML
    private TextField emailField;

    @FXML
    private TextField loginField;

    @FXML
    private Label messageLabel;

    @FXML
    private Button backBtn;

    @FXML
    private Button logoutBtn;

    @FXML
    private Button homeBtn;

    Service service = new Service();

    @FXML
    void deleteUserAction(ActionEvent event) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Czy jesteś pewny/a, że chcesz usunąć"
                + " tego użytkownika?", ButtonType.YES, ButtonType.CANCEL);
        alert.setHeaderText("Usuwanie użytkownika");
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            service.deleteUser(peselField.getText());
            service.navigateToPath(Path.userList);
        }
    }


    @FXML
    void backAction(ActionEvent event) {
        service.navigateToPath(Path.userList);

    }

    @FXML
    void editUserAction(ActionEvent event) {

        if (isInputValid()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Czy jesteś pewny/a, że chcesz nadpisać"
                    + " dane tego użytkownika?", ButtonType.YES, ButtonType.CANCEL);
            alert.setHeaderText("Edytowanie użytkownika");
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                updateUser();
                service.navigateToPath(Path.userList);
            }
        }
    }

    @FXML
    void homeAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    public void setUserDetails(){
        Profile userForDetails = AppSession.getInstance().getUserForDetails();
        nameField.setText(userForDetails.getFirstName());
        lastNameField.setText(userForDetails.getLastName());
        peselField.setText(userForDetails.getPESEL());
        phoneField.setText(userForDetails.getPhone());
        cityField.setText(userForDetails.getCity());
        streetField.setText(userForDetails.getAddress());
        postalcodeField.setText(userForDetails.getPostalCode());
        emailField.setText(userForDetails.getEmail());
        loginField.setText(service.getUser(userForDetails).getUserName());
        roleBox.setValue(Role.valueOf(String.valueOf(service.getUser(userForDetails).getRole())));

    }

    public void updateUser(){
        service.updateUser(nameField.getText(), lastNameField.getText(), peselField.getText(),
                phoneField.getText(), emailField.getText(), cityField.getText(), streetField.getText(), postalcodeField.getText(),
                loginField.getText());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setUserDetails();
    }

    private boolean isInputValid() {
        messageLabel.setText("");
        Pattern emailPattern = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)"
                + "*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")"
                + "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])"
                + "?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])"
                + "|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
        Pattern peselPattern = Pattern.compile("^\\d{11}$");
        Pattern phonePattern = Pattern.compile("(?<!\\w)(\\(?(\\+|00)?48\\)?)?[ -]?\\d{3}[ -]?\\d{3}[ -]?\\d{3}(?!\\w)");
        Pattern loginPattern = Pattern.compile("^\\w{3,}$");
        Pattern passwordPattern = Pattern.compile("^\\w{6,}$");

        if(!(peselPattern.matcher(peselField.getText()).matches())){
            messageLabel.setText("Nieprawidłowy numer PESEL");
            return false;
        }
        else if(!(phonePattern.matcher(phoneField.getText()).matches())){
            messageLabel.setText("Nieprawidłowy numer telefonu");
            return false;
        }
        else if(!(emailPattern.matcher(emailField.getText()).matches())){
            messageLabel.setText("Nieprawidłowy adres e-mail");
            return false;
        }
        else if(!(loginPattern.matcher(loginField.getText()).matches())){
            messageLabel.setText("Login musi zawierać min. 3 znaki");
            return false;
        }
        return true;
    }
}
