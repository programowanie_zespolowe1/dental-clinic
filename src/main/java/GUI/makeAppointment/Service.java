package main.java.GUI.makeAppointment;

import main.java.database.Connector;
import main.java.database.entity.*;
import main.java.database.enums.Role;
import main.java.database.enums.ToothState;
import main.java.entry.AppSession;
import main.java.navigator.Path;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Service {

    /**
     * Gets dentist from database for given patient Profile.
     *
     * @param profile   patient Profile
     * @return dentist Profile
     */
    public Profile getDentist(Profile profile) {
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> dentistProfileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> dentistProfileRoot = dentistProfileCriteriaQuery.from(Profile.class);

            dentistProfileCriteriaQuery.select(dentistProfileRoot).where(builder.equal(dentistProfileRoot.get("GUID"), profile.getDefaultDentistGuid()));
            Profile dentistProfile = (Profile) session.createQuery(dentistProfileCriteriaQuery).getSingleResult();

            return dentistProfile;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Gets Schedules from database for given Profile.
     *
     * @param profile   dentist Profile
     * @return  list of schedules for Profile
     */
    public List<Schedule> getScheduleForDentist(Profile profile) {
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Schedule> scheduleCriteriaQuery = builder.createQuery(Schedule.class);
            Root<Schedule> scheduleRoot = scheduleCriteriaQuery.from(Schedule.class);

            scheduleCriteriaQuery.select(scheduleRoot).where(builder.equal(scheduleRoot.get("profile"), profile));
            List<Schedule> scheduleList = session.createQuery(scheduleCriteriaQuery).getResultList();

            return scheduleList;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Gets all Users with Dentist Role and their Profiles.
     *
     * @return list of dentists Profiles
     */
    public List<Profile> loadAllDentists() {

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<User> dentistUsersCriteriaQuery = builder.createQuery(User.class);
            Root<User> dentistUsersRoot = dentistUsersCriteriaQuery.from(User.class);

            dentistUsersCriteriaQuery.select(dentistUsersRoot).where(builder.equal(dentistUsersRoot.get("role"), Role.Dentysta));
            List<User> dentistUsersList = session.createQuery(dentistUsersCriteriaQuery).getResultList();

            CriteriaQuery<Profile> dentistProfilesCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> dentistProfilesRoot = dentistProfilesCriteriaQuery.from(Profile.class);

            List<Profile> dentistProfilesList = new ArrayList<>();
            for(int i = 0; i < dentistUsersList.size(); i++){

                dentistProfilesCriteriaQuery.select(dentistProfilesRoot).where(builder.equal(dentistProfilesRoot.get("userGuid"), dentistUsersList.get(i)));
                Query sessionFindDentists = session.createQuery(dentistProfilesCriteriaQuery);
                dentistProfilesList.add((Profile) sessionFindDentists.getSingleResult());
            }

            return dentistProfilesList;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }


    /**
     * Gets all Visits for given Profile and day from database.
     *
     * @param dentist
     * @param date
     * @return
     */
    public List<Visit> loadVisitsForDay(Profile dentist, LocalDate date) {

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Visit> visitsCriteriaQuery = builder.createQuery(Visit.class);
            Root<Visit> visitsRoot = visitsCriteriaQuery.from(Visit.class);

            Predicate[] predicates = new Predicate[2];
            predicates[0] = builder.equal(visitsRoot.get("visitDate"), date);
            predicates[1] = builder.equal(visitsRoot.get("dentistProfile"), dentist);

            visitsCriteriaQuery.select(visitsRoot).where(predicates);
            List<Visit> visitsList = session.createQuery(visitsCriteriaQuery).getResultList();

            return visitsList;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Adds a Visit to database with given parameters.
     * @param visitDate     date of the visit
     * @param visitTime     time of the visit
     * @param dentistProfile    dentistProfile Profile
     * @param patientProfile    patient Profile
     */
    public void addVisit(LocalDate visitDate, LocalTime visitTime, Profile dentistProfile, Profile patientProfile) {

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Visit visit = new Visit(visitDate, visitTime,dentistProfile, patientProfile);
            session.save(visit);
            tx.commit();

        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;

        } finally {
            session.close();
        }
    }

    /**
     *  Navigates to given FXML path using Paths enum.
     *
     *  @param path Path enum to FXML file
     */
    public void navigateToPath(Path path){
        AppSession appSession = AppSession.getInstance();
        try{
            appSession.getNavigator().navigateTo(path);
        }
        catch(Exception e){
            throw e;
        }
    }

}
