package main.java.GUI.makeAppointment;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.net.URL;
import javafx.scene.text.Text;
import main.java.database.entity.Profile;
import main.java.database.entity.Schedule;
import main.java.database.entity.Visit;
import main.java.entry.AppSession;
import main.java.navigator.Path;

import java.time.format.DateTimeFormatter;
import java.time.LocalDate;

public class Controller implements Initializable {

    @FXML
    private Text patientName;

    @FXML
    private GridPane calendarVisit;

    @FXML
    private TextField monthField;

    @FXML
    private TextField yearField;

    @FXML
    private ComboBox<String> dentistBox;

    @FXML
    private TextField hourField;

    @FXML
    private TextField minuteField;

    @FXML
    private Label messageLabel;

    @FXML
    private TableView<TimeInTable> freeVisitTimeList;

    @FXML
    private TableColumn<TimeInTable, String> timeList;

    private LocalDate localDate;

    private Service service = new Service();

    private LocalDate appointmentDate;

    private ObservableList<TimeInTable> visitList;

    public void initialize(URL url, ResourceBundle resourceBundle) {

        AppSession appSession = AppSession.getInstance();
        localDate = LocalDate.now();
        loadDentists();
        patientName.setText(appSession.getPatientForVisit().getFirstName() + " " + appSession.getPatientForVisit().getLastName());
        dentistBox.setValue(service.getDentist(appSession.patientForVisit).getFirstName() + " "
                + service.getDentist(appSession.patientForVisit).getLastName());
        monthField.setText(String.valueOf(localDate.getMonthValue()));
        yearField.setText(String.valueOf(localDate.getYear()));
    }

    /**
     * Checks if hour from textfields is correct.
     *
     * @return true or false
     */
    public boolean checkTime(){
        String hour = hourField.getText();
        String minute = minuteField.getText();
        String inputTimeString = hour+":"+minute;
        try {
            LocalTime.parse(inputTimeString);
            return true;
        } catch (DateTimeParseException | NullPointerException e) {
            messageLabel.setText("Nieprawidłowa godzina");
            return false;
        }
    }

    public void loadDentists(){
        List<Profile> dentistsList = service.loadAllDentists();
        for(int i = 0; i < dentistsList.size(); i++) {
            dentistBox.getItems().add(dentistsList.get(i).getFirstName() + " " + dentistsList.get(i).getLastName());
        }
    }

    /**
     * Displays free hours for particular day.
     * This method creates list of hours from 8:00 to 21:00 with half hour intervals and gets
     * list of visits for dentist (saved in AppSession) if hour for visit is occupied
     * then it is removed from list.
     *
     * @param dayOfWeek name of day of week
     * @param day   day as number
     */
    public void loadFreeTimeForVisit(String dayOfWeek, String day){

        freeVisitTimeList.getItems().clear();
        Profile dentist = service.loadAllDentists().get(dentistBox.getSelectionModel().getSelectedIndex());
        List<Schedule> scheduleList = service.getScheduleForDentist(dentist);
        LocalDate dateForVisit = getAppointmentDate(day, monthField.getText(), yearField.getText());
        timeList.setText(String.valueOf(dateForVisit));
        List<Visit> visitListForDay = service.loadVisitsForDay(dentist, dateForVisit);
        int index = -1;
        for(int i = 0; i < scheduleList.size(); i++){
            if(scheduleList.get(i).getDayOfWeek().equals(dayOfWeek)){
                index = i;
            }
        }
        if(index != -1){

            Time startHour = scheduleList.get(index).getStartTime();
            Time endHour = scheduleList.get(index).getEndTime();

            visitList = FXCollections.observableArrayList();

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

            Calendar cal = Calendar.getInstance();
            cal.setTime(startHour);

            while(cal.getTime().before(endHour)){
                TimeInTable timeInTable = new TimeInTable(sdf.format(cal.getTime()));
                visitList.add(timeInTable);
                cal.add(Calendar.MINUTE, 30);
                for(int j = 0; j < visitListForDay.size(); j++){
                    if(visitListForDay.get(j).getVisitTime().toString().equals(timeInTable.getTime())){
                        visitList.remove(timeInTable);
                    }
                }
            }
        }

        timeList.setCellValueFactory(new PropertyValueFactory<TimeInTable, String>("time"));
        freeVisitTimeList.getItems().addAll(visitList);
    }

    /**
     * Converts String values of day, month and year to LocalDate.
     *
     * @param day   day as number
     * @param month month as number
     * @param year  year as number
     * @return date as LocalDate
     */
    public LocalDate getAppointmentDate(String day, String month, String year) {

        String date;

        if (day.trim().length() == 1) {
            day = "0" + day;
        }
        if (month.trim().length() == 1) {
            month = "0" + month;
        }

        date = year + "-" + month + "-" + day;

        appointmentDate = LocalDate.parse(date);

        return appointmentDate;
    }

    /**
     * Sets date from textfields and displays calendar for given month and year.
     * @param event
     */
    @FXML
    void confirmDateAction(ActionEvent event) {
        messageLabel.setText("");
        if(checkTime()){
            int year = Integer.valueOf(yearField.getText());
            int month = Integer.valueOf(monthField.getText());
            LocalDate date = LocalDate.of(year, month, 1);
            setDateShow(date);
        }
    }


    @FXML
    void backAction(ActionEvent event) {
        service.navigateToPath(Path.patientList);
    }

    @FXML
    void confirmAction(ActionEvent event) {

        TimeInTable timeInTable = freeVisitTimeList.getSelectionModel().getSelectedItem();
        LocalTime timeFromTable = LocalTime.parse(timeInTable.getTime());

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Czy chcesz zapisać daną wizytę?  ",
                ButtonType.YES, ButtonType.CANCEL);
        alert.setHeaderText(timeInTable.getTime() + " " + appointmentDate);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {

            Profile dentist = service.getDentist(AppSession.getInstance().patientForVisit);
            service.addVisit(appointmentDate, timeFromTable,
                    dentist, AppSession.getInstance().patientForVisit);
            AppSession.getInstance().setDentistForCalendar(dentist);
            AppSession.getInstance().setAppointmentDate(appointmentDate);
            service.navigateToPath(Path.appointmentList);
        }

    }

    @FXML
    void homeAction(ActionEvent event) {
        service.navigateToPath(Path.landing);
    }

    @FXML
    void logoutAction(ActionEvent event) {
        service.navigateToPath(Path.login);
    }

    /**
     * Decreases number of the month in textfield by one (not less than 1).
     *
     * @param event
     */
    @FXML
    void monthDownAction(ActionEvent event) {

        try {
            int monthValue = Integer.parseInt(monthField.getText());

            if (monthValue > 1) {

                monthValue--;

                String x = Integer.toString(monthValue);

                monthField.setText(x);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    /**
     * Increases number of the month in textfield by one(not higher than 12).
     *
     * @param event
     */
    @FXML
    void monthUpAction(ActionEvent event) {

        try {
            int monthValue = Integer.parseInt(monthField.getText());

            if (monthValue < 12) {

                monthValue++;

                String x = Integer.toString(monthValue);

                monthField.setText(x);
            }
        } catch (NumberFormatException e) {
        e.printStackTrace();
        }

    }

    /**
     * Decreases number of the year in textfield by one.
     *
     * @param event
     */
    @FXML
    void yearDownAction(ActionEvent event) {

        try {
            int yearValue = Integer.parseInt(yearField.getText());

            yearValue--;

            String x = Integer.toString(yearValue);

            yearField.setText(x);
        } catch (NumberFormatException e) {
        e.printStackTrace();
        }
    }

    /**
     * Increases number of the year in textfield by one.
     *
     * @param event
     */
    @FXML
    void yearUpAction(ActionEvent event) {

        try{
        int yearValue = Integer.parseInt(yearField.getText());

        yearValue++;

        String x = Integer.toString(yearValue);

        yearField.setText(x);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    /**
     * Displays days of the month as buttons.
     * From first day of the month pulls day of the week and the following days.
     * If day of the week is in list of schedules for dentist then
     * it adds to button action loadFreeTimeForVisit.
     * If visit is available for particular day (not in list of occupied visits for day)
     * then it sets button color as green.
     *
     * @param localDate date
     *
     */
    public void setDateShow(LocalDate localDate) {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy MM");

        LocalDate firstDay = localDate.withDayOfMonth(1);
        int dateIndex = firstDay.getDayOfWeek().getValue();

        int daysOfMonth = getMonthDays(localDate.getYear(), localDate.getMonthValue());

        int days = 1;

        calendarVisit.getChildren().clear();
        String hour = hourField.getText();
        String minute = minuteField.getText();
        String inputTimeString = hour+":"+minute;
        List<Schedule> scheduleList = service.getScheduleForDentist(service.loadAllDentists().get(dentistBox.getSelectionModel().getSelectedIndex()));
        Profile dentistProfile = service.loadAllDentists().get(dentistBox.getSelectionModel().getSelectedIndex());

        for (int column = dateIndex-1; column <= 6; column++) {

            Button button = new Button(String.valueOf(days));
            button.setPrefSize(30, 100);

            for(int i = 0; i < scheduleList.size(); i++ ){

                LocalDate dateInCalendar = getAppointmentDate(String.valueOf(days), monthField.getText(), yearField.getText());
                if(scheduleList.get(i).getDayOfWeek().equals(dateInCalendar.getDayOfWeek().toString())){

                    button.setOnAction(e -> loadFreeTimeForVisit(dateInCalendar.getDayOfWeek().toString(), button.getText()));

                    button.setStyle("-fx-background-color: #66ff66; ");

                    List<Visit> visitsList = service.loadVisitsForDay(dentistProfile, getAppointmentDate(String.valueOf(days), monthField.getText(),
                            yearField.getText()));

                    for(int j = 0; j < visitsList.size(); j++){
                        if(visitsList.get(j).getVisitTime().toString().equals(inputTimeString)){
                            button.setStyle(null);
                        }
                    }

                }
            }
            calendarVisit.add(button, column, 0);
            days++;
            daysOfMonth--;
        }

        for (int row = 1; row <= 6; row++) {
            for (int column = 0; column <= 6; column++) {
                Button button = new Button(String.valueOf(days));
                button.setPrefSize(30, 100);

                for(int i = 0; i < scheduleList.size(); i++ ){

                    LocalDate dateInCalendar = getAppointmentDate(String.valueOf(days), monthField.getText(), yearField.getText());
                    if(scheduleList.get(i).getDayOfWeek().equals(dateInCalendar.getDayOfWeek().toString())){

                        button.setOnAction(e -> loadFreeTimeForVisit(dateInCalendar.getDayOfWeek().toString(), button.getText()));

                        button.setStyle("-fx-background-color: #66ff66; ");

                        List<Visit> visitsList = service.loadVisitsForDay(dentistProfile, getAppointmentDate(String.valueOf(days), monthField.getText(),
                                yearField.getText()));

                        for(int j = 0; j < visitsList.size(); j++){
                            if(visitsList.get(j).getVisitTime().toString().equals(inputTimeString)){
                                button.setStyle(null);
                            }
                        }

                    }
                }
                calendarVisit.add(button, column, row);
                days++;
                daysOfMonth--;

                if (0 == daysOfMonth) {
                    break;
                }
            }

            if (0 == daysOfMonth) {
                break;
            }
        }

    }

    /**
     * Calculates number of days for given month and year.
     *
     * @param year number of year
     * @param month number of month
     * @return number of days in the month
     */
    public int getMonthDays(int year, int month) {

        int monthDays = 0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                monthDays = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                monthDays = 30;
                break;
            case 2:
                if ((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0)) {
                    monthDays = 29;
                } else {
                    monthDays = 28;
                }
                break;
        }
        return monthDays;
    }

    public class TimeInTable{

        public String time;

        public TimeInTable(String time){
            this.time = time;
        }

        public String getTime() {
            return time;
        }

    }
}
