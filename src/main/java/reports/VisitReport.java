package main.java.reports;

import com.dc.pdfgenerator.Report;
import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.User;
import main.java.database.entity.Visit;
import main.java.entry.AppSession;
import main.java.entry.Main;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VisitReport extends Report {

    public String getPdfName(){return "Lista wizyt " + AppSession.getInstance().getAppointmentDate().toString();}

    public String getPdfFileName(){return "visitReport";}

    public List<Visit> loadVisists(Profile dentistProfile, LocalDate date) {

        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Visit> visitCriteriaQuery = builder.createQuery(Visit.class);
            Root<Visit> visitRoot = visitCriteriaQuery.from(Visit.class);

            Predicate[] predicates = new Predicate[2];
            predicates[0] = builder.equal(visitRoot.get("visitDate"), date);
            predicates[1] = builder.equal(visitRoot.get("dentistProfile"), dentistProfile);

            visitCriteriaQuery.select(visitRoot).where(predicates);
            Query sessionFindVisitQuery = session.createQuery(visitCriteriaQuery);
            List<Visit> visitsList = sessionFindVisitQuery.getResultList();
            return visitsList;
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }


    public void createPdf(String profileNameAndLastName) throws FileNotFoundException {


        List<Visit> visitList = loadVisists(AppSession.getInstance().getDentistForCalendar(), AppSession.getInstance().getAppointmentDate());
        List<Map<String, String>> mapOfVisit = new ArrayList<>();

        for (Visit visit:
                visitList) {
            mapOfVisit.add(visit.toMap());
        }

        this.createDocument(Main.PDF_DEST);
        this.addTitle(getPdfName());
        this.addTableAsMap(mapOfVisit);
        this.generatedBy(profileNameAndLastName);
        this.closeDocument();
    }

    public void generateVisitReport(String profileNameAndLastName) throws FileNotFoundException {
        this.createPdf(profileNameAndLastName);

    }

}