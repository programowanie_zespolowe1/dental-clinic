package main.java.reports;

import com.dc.pdfgenerator.Report;
import main.java.database.entity.Profile;
import main.java.entry.Main;

import java.io.FileNotFoundException;
import java.util.List;

public class SampleReport extends Report {

    public String getPdfFileName() {
        return "sample report";
    }

    public String getPdfName() {
        return "sample pdf report";
    }

    public void createPdf(String generatedBy) throws FileNotFoundException {
        this.createDocument(Main.PDF_DEST);
        this.addTitle(getPdfName());
        this.generatedBy("Sample user");
        this.closeDocument();
    }
}
