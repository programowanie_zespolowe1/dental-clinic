package main.java.reports;

import main.java.GUI.patientCard.Service;
import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.ToothHistory;
import main.java.database.extension.ToothHistoryNumberComparator;
import main.java.entry.AppSession;
import main.java.entry.Main;
import com.dc.pdfgenerator.*;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.FileNotFoundException;
import java.util.*;

public class ToothHistoryReport extends Report{

    public String getPdfName(){return "Historia leczenia zeba";}

    public String getPdfFileName(){return "toothHistoryReport";}

//    public List<ToothHistory> loadPatientsToothHistories(Profile profile) {
//        Session session = Connector.openSession();
//        Transaction tx = null;
//        try {
//            tx = session.beginTransaction();
//            CriteriaBuilder builder = session.getCriteriaBuilder();
//            CriteriaQuery<ToothHistory> toothHistoryCriteriaQuery = builder.createQuery(ToothHistory.class);
//            Root<ToothHistory> toothHistoryRoot = toothHistoryCriteriaQuery.from(ToothHistory.class);
//
//            toothHistoryCriteriaQuery.select(toothHistoryRoot).where(builder.equal(toothHistoryRoot.get("profile"), profile));
//
//            return session.createQuery(toothHistoryCriteriaQuery).getResultList();
//        } catch (Exception e) {
//            if (tx != null) tx.rollback();
//            throw e;
//        }
//    }
 Service service = new Service();
    private Profile getProfileByGUID(String GUID) {
        Session session = Connector.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("GUID"), GUID));

            return session.createQuery(profileCriteriaQuery).getSingleResult();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            throw e;
        }
    }

    public void createPdf(String profileGUID) throws FileNotFoundException {
        Profile profile = this.getProfileByGUID(profileGUID);

        List<ToothHistory> toothHistoryList= service.loadToothHistory(AppSession.getInstance().getToothNumber());

//        List<ToothHistory> toothHistoryList = loadPatientsToothHistories(profile);
        List<Map<String, String>> mapOfHistory = new ArrayList<>();

        toothHistoryList.sort(new ToothHistoryNumberComparator());

        for (ToothHistory toothHistory:
             toothHistoryList) {
            mapOfHistory.add(toothHistory.toMap());
        }

        this.createDocument(Main.PDF_DEST);
        this.addTitle(getPdfName());
        this.addTableAsMap(mapOfHistory);
        this.generatedBy(profileGUID);
        this.closeDocument();
    }

    public void generateToothHistoryPdf(String profileGUID) throws FileNotFoundException {
        this.createPdf(profileGUID);
    }

}
