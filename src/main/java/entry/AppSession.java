package main.java.entry;

import main.java.GUI.patientList.ToothHistoryList;
import main.java.database.entity.Profile;
import main.java.database.entity.ToothHistory;
import main.java.database.entity.User;
import main.java.navigator.Navigator;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class AppSession {
    private static AppSession INSTANCE;

    private User loggedUser;
    private Profile loggedUserProfile;

    private Navigator navigator;

    public LocalDate appointmentDate;

    public Profile userForDetails;

    public Profile editFromPatientsList;

    public Profile patientForVisit;

    public Profile dentistForCalendar;

    private Profile choosenForToothHistory;

    private ToothHistory addToToothHistory;

    private ToothHistory editToToothHistory;

    private Integer toothNumber;

    private String jaw;


    /**
     * AppSession constructor
     */
    private AppSession() {
        this.navigator = new Navigator();
    }

    /**
     * Returns instance of AppSession
     *
     * @return instance of AppSession
     */
    public static AppSession getInstance() {
        if (INSTANCE == null)
            INSTANCE = new AppSession();

        return INSTANCE;
    }

    /**
     * Checks if user is logged
     *
     * @return boolean
     */
    public boolean isUserLogged() {
        return !this.loggedUser.equals(null);
    }

    /**
     * @return Navigator object
     */
    public Navigator getNavigator() {
        return navigator;
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }

    public Profile getLoggedUserProfile() { return loggedUserProfile; }

    public void setLoggedUserProfile(Profile loggedUserProfile) {
        this.loggedUserProfile = loggedUserProfile;
    }

    public void setAppointmentDate(String day, String month, String year) {

        String date;

        if (day.trim().length() == 1) {
            day = "0" + day;
        }
        if (month.trim().length() == 1) {
            month = "0" + month;
        }

        date = year + "-" + month + "-" + day;

        try {
            appointmentDate = LocalDate.parse(date);
        } catch (DateTimeParseException exc) {
        }

    }

    public void setAppointmentDate(LocalDate visitDate) { appointmentDate = visitDate; }

    public Profile getChoosenForToothHistory() {
        return choosenForToothHistory;
    }

    public void setChoosenForToothHistory(Profile choosenForToothHistory) {
        this.choosenForToothHistory = choosenForToothHistory;
    }


    public LocalDate getAppointmentDate() {
        return appointmentDate;
    }

    public void setUserForDetails(Profile profile) {
        userForDetails = profile;
    }

    public Profile getUserForDetails() {
        return userForDetails;
    }

    public void setEditFromPatientsList(Profile profile) {
        editFromPatientsList = profile;
    }

    public Profile getEditFromPatientsList() {
        return editFromPatientsList;
    }

    public void setPatientForVisit(Profile profile) {
        patientForVisit = profile;
    }

    public Profile getPatientForVisit() {
        return patientForVisit;
    }

    public void setDentistForCalendar(Profile dentist) { dentistForCalendar = dentist; }

    public Profile getDentistForCalendar() { return dentistForCalendar; }

    public void setAddToToothHistory(ToothHistory toothHistory) { addToToothHistory = toothHistory; }

    public ToothHistory getAddToToothHistory() { return addToToothHistory; }

    public ToothHistory getEditToToothHistory() { return editToToothHistory; }

    public void setEditToToothHistory(ToothHistory toothHistory) { editToToothHistory = toothHistory; }

    public Integer getToothNumber() {
        return toothNumber;
    }

    public void setToothNumber(Integer toothNumber) {
        this.toothNumber = toothNumber;
    }

    public String getJaw() {
        return jaw;
    }

    public void setJaw(String jaw) {
        this.jaw = jaw;
    }


}
