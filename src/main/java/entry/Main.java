package main.java.entry;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import main.java.database.Connector;
import main.java.navigator.Path;
import org.hibernate.Session;

import java.io.File;
import java.net.MalformedURLException;

public class Main extends Application {

    public static final String PDF_DEST = System.getProperty("user.home") + "/Desktop/raporty";

    @Override
    public void start(Stage primaryStage) throws Exception {
        AppSession appSession = AppSession.getInstance();

        appSession.getNavigator().setRoot(FXMLLoader.load(Path.title.getPath()));

        AppSession.getInstance().setJaw("none");


        this.setDefaultStage(primaryStage, appSession.getNavigator().getRoot());
        appSession.getNavigator().setStage(primaryStage);

        primaryStage.show();

        appSession.getNavigator().navigateTo(Path.login);

        Connector.openSession();
    }

    /**
     * Sets default stage for application
     *
     * @param stage javafx.stage.Stage
     * @param root javafx.scene.Parent
     */
    public void setDefaultStage(Stage stage, Parent root) throws MalformedURLException {
        stage.setTitle("Dental clinic");
        var f = new File("src/main/java/asssets/icons/main.png");
        stage.getIcons().add(new Image(f.toURI().toString()));
        stage.setScene(new Scene(root, 800, 600));
    }


    public static void main(String[] args) {
        launch(args);
    }
}
