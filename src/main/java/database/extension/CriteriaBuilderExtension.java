package main.java.database.extension;

import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class CriteriaBuilderExtension{
    public CriteriaBuilder cb;
    public CriteriaQuery cq;
    public Root r;

    public CriteriaBuilderExtension(Session s, Object c) {
        this.cb = s.getCriteriaBuilder();
        this.cq = this.cb.createQuery(c.getClass());
        this.r = (Root) this.cq.from(c.getClass());
    }

    public CriteriaBuilder getCriteriaBuilder() {
        return cb;
    }

    public CriteriaQuery getCriteriaQuery() {
        return cq;
    }

    public Root getRoot() {
        return r;
    }
}
