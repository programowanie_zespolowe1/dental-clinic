package main.java.database.extension;

import main.java.database.entity.ToothHistory;

import java.util.Comparator;

public class ToothHistoryNumberComparator implements Comparator<ToothHistory> {
    public int compare(ToothHistory th1, ToothHistory th2) {
        return Integer.compare(th1.getToothNumber(), th2.getToothNumber());
    }
}
