package main.java.database.entity;

import main.java.database.enums.Role;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator( name = "UUID", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name = "userName", unique = true)
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private Role role;

    @OneToOne()
    @JoinColumn(name = "profileGuid", referencedColumnName = "id", unique = true)
    private Profile profile;

    public User() {}

    public User(String userName, String password, Role role, Profile profile) {
        this.userName = userName;
        this.password = password;
        this.role = role;
        this.profile = profile;
    }

    public String getGUID() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
