package main.java.database.entity;

import main.java.database.enums.ToothState;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "ToothHistory")
public class ToothHistory {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator( name = "UUID", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name="toothNumber")
    private Integer toothNumber;

    @Column(name = "visitDate")
    private Date visitDate;

    @Column(name = "history")
    private String history;

    @Column(name = "xRayPicture")
    private String xRayPicture;

    @Column(name = "toothState")
    private ToothState toothState;

    @ManyToOne
    @JoinColumn(name = "profileGuid", referencedColumnName = "id")
    private Profile profile;

    public ToothHistory(Date visitDate, String history, String xRayPicture, ToothState toothState) {
        this.visitDate = visitDate;
        this.history = history;
        this.xRayPicture = xRayPicture;
        this.toothState = toothState;
    }

    public ToothHistory(Integer toothNumber, Date visitDate, String history, String xRayPicture, ToothState toothState, Profile profile) {
        this.toothNumber = toothNumber;
        this.visitDate = visitDate;
        this.history = history;
        this.xRayPicture = xRayPicture;
        this.toothState = toothState;
        this.profile = profile;
    }

    public ToothHistory(Integer toothNumber, Date addDate, ToothState toothState, Profile profile) {
        this.toothNumber = toothNumber;
        this.visitDate = addDate;
        this.toothState = toothState;
        this.profile = profile;
    }

    public ToothHistory() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getToothNumber() {
        return toothNumber;
    }

    public void setToothNumber(Integer toothNumber) {
        this.toothNumber = toothNumber;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getxRayPicture() {
        return xRayPicture;
    }

    public void setxRayPicture(String xRayPicture) {
        this.xRayPicture = xRayPicture;
    }

    public ToothState getToothState() {
        return toothState;
    }

    public void setToothState(ToothState toothState) {
        this.toothState = toothState;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Map<String, String> toMap(){
        Map<String, String> map = new HashMap<>();
        map.put("Tooth id: ", this.toothNumber.toString());
        if (this.history != null && !this.history.isEmpty()) {
            map.put("History: ", this.history);
        } else {
            map.put("History: ", "NULL");
        }

        return map;
    }
}
