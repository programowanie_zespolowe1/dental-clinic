package main.java.database.entity.comparators;

import main.java.database.entity.ToothHistory;

import java.util.Comparator;

public class ToothHistoryDateComparator implements Comparator<ToothHistory> {
    @Override
    public int compare(ToothHistory o1, ToothHistory o2) {
        return o2.getVisitDate().compareTo(o1.getVisitDate());
    }
}
