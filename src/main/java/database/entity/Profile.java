package main.java.database.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Profile")
public class Profile {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator( name = "UUID", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column(name = "id", updatable = false, nullable = false)
    private String GUID;

    @Column(name = "firstName")
    private String firstName;
    
    @Column(name = "lastName")
    private String lastName;
    
    @Column(name = "PESEL")
    private String PESEL;
    
    @Column(name = "city")
    private String city;
    
    @Column(name = "address")
    private String address;
    
    @Column(name = "postalCode")
    private String postalCode;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "phone")
    private String phone;

    @Column(name = "defaultDentistGuid")
    private String defaultDentistGuid;

    @OneToOne()
    @JoinColumn(name = "userGuid", referencedColumnName = "id", unique = true)
    private User userGuid;

    @OneToMany
    private List<Schedule> schedule;

    @OneToMany
    private List<ToothHistory> toothHistory;

    @OneToMany
    private List<Visit> visits;

    public Profile() {}

    public Profile(String firstName, String lastName, String PESEL, String city, String address, String postalCode, String email, String phone, String defaultDentistGuid) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.PESEL = PESEL;
        this.city = city;
        this.address = address;
        this.postalCode = postalCode;
        this.email = email;
        this.phone = phone;
        this.defaultDentistGuid = defaultDentistGuid;
    }

    public Profile(String firstName, String lastName, String PESEL, String city, String address, String postalCode, String email, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.PESEL = PESEL;
        this.city = city;
        this.address = address;
        this.postalCode = postalCode;
        this.email = email;
        this.phone = phone;
    }

    public Profile(String firstName, String lastName, String PESEL, String city, String address, String postalCode, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.PESEL = PESEL;
        this.city = city;
        this.address = address;
        this.postalCode = postalCode;
        this.phone = phone;
    }

    public Profile(String firstName, String lastName, String PESEL, String city, String address, String postalCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.PESEL = PESEL;
        this.city = city;
        this.address = address;
        this.postalCode = postalCode;
    }

    public String getGUID() { return this.GUID; }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPESEL() {
        return PESEL;
    }

    public void setPESEL(String PESEL) {
        this.PESEL = PESEL;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getDefaultDentistGuid() {
        return defaultDentistGuid;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setDefaultDentistGuid(String defaultDentistGuid) {
        this.defaultDentistGuid = defaultDentistGuid;
    }


    /**
     * Use on instance of Profile when creating User, to make relation with User
     * @param u User - user to reference in table.
     */
    public void setUserGuid(User u) {
        this.userGuid = u;
    }
}
