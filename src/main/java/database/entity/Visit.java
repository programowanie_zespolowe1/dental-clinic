package main.java.database.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "Visit")
public class Visit {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator( name = "UUID", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name = "visitDate")
    private LocalDate visitDate;

    @Column(name = "visitTime")
    private LocalTime visitTime;

    @Column( name = "isActive")
    private boolean isActive;

    @ManyToOne
    @JoinColumn(name = "dentistProfileGuid", referencedColumnName = "id")
    private Profile dentistProfile;

    @ManyToOne
    @JoinColumn(name = "patientProfileGuid", referencedColumnName = "id")
    private Profile patientProfile;

    public Visit(LocalDate visitDate, LocalTime visitTime, Profile dentistProfile, Profile patientProfile) {
        this.visitDate = visitDate;
        this.visitTime = visitTime;
        this.isActive = true;
        this.dentistProfile = dentistProfile;
        this.patientProfile = patientProfile;
    }

    public Visit() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getVisitDate() { return visitDate; }

    public void setVisitDate(LocalDate visitDate) { this.visitDate = visitDate; }

    public LocalTime getVisitTime() { return visitTime; }

    public void setVisitTime(LocalTime visitTime) { this.visitTime = visitTime; }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Profile getDentistProfile() {
        return dentistProfile;
    }

    public void setDentistProfile(Profile dentistProfile) {
        this.dentistProfile = dentistProfile;
    }

    public Profile getPatientProfile() {
        return patientProfile;
    }

    public void setPatientProfile(Profile patientProfile) {
        this.patientProfile = patientProfile;
    }

    public Map<String, String> toMap(){
        Map<String, String> map = new HashMap<>();
        map.put("Dentysta: ", this.dentistProfile.getFirstName() + " " + this.dentistProfile.getLastName());
        map.put("PESEL Pacjenta: ", this.patientProfile.getPESEL());
        map.put("Pacjent: ", this.patientProfile.getFirstName() + " " + this.patientProfile.getLastName());
        map.put("Godzina wizyty: ", String.valueOf(this.visitTime));
        return map;
    }
}
