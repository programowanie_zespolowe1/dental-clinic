package main.java.database;

import main.java.database.entity.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

public class Connector {

    private static Configuration configuration;
    private static final SessionFactory SESSION_FACTORY;

    public static SessionFactory getSessionFactory() {
        return SESSION_FACTORY;
    }

    static {
        try {
            createConfiguration();

            createAnnotations();

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .build();

            SESSION_FACTORY = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * @return Hibernate session
     * @throws HibernateException
     */
    public static Session openSession() throws HibernateException {
        return SESSION_FACTORY.openSession();
    }

    public static void closeSession() throws HibernateException {
        SESSION_FACTORY.close();
    }

    /**
     * Creates configuration for Hibernate
     */
    private static void createConfiguration() {
        configuration = new Configuration();

        configuration.setProperty(Environment.DRIVER, "com.mysql.jdbc.Driver");
        configuration.setProperty(Environment.DIALECT, "org.hibernate.dialect.MySQL8Dialect");
        configuration.setProperty(Environment.URL, "jdbc:mysql://localhost:3306/dental?createDatabaseIfNotExist=true");

        configuration.setProperty(Environment.USER, "root");
        configuration.setProperty(Environment.PASS, "");

        configuration.setProperty(Environment.SHOW_SQL, "true");
        configuration.setProperty(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
//        Comments under are for: creating database, updating it, and production
//        configuration.setProperty(Environment.HBM2DDL_AUTO, "create");
//        configuration.setProperty(Environment.HBM2DDL_AUTO, "update");
        configuration.setProperty(Environment.HBM2DDL_AUTO, "validate");
    }

    /**
     * Creates annotations to mapping classes for Hibernate
     */
    private static void createAnnotations() {
        configuration.addAnnotatedClass(ToothHistory.class);
        configuration.addAnnotatedClass(Profile.class);
        configuration.addAnnotatedClass(Schedule.class);
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(Visit.class);
    }
}
