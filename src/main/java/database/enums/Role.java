package main.java.database.enums;

public enum Role {
    Pacjent,
    Dentysta,
    Higienistka,
    Recepcjonista,
    Administrator
}
