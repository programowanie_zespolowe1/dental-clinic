package main.java.database.enums;

public enum ToothState {
    NONE,       //white
    GOOD,       //green
    IN_PROGRESS,    //yellow
    FOR_TREATMENT,  //red colour
    REMOVED;        //black
}
