package main.java.navigator;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.java.navigator.Path;

import java.io.IOException;

public class Navigator {
    private Parent root;
    private Stage stage;

    public Navigator() {}

    /**
     * Navigates to given FXML path using Paths enum.
     *
     * @param path Path enum to FXML file
     */
    public void navigateTo( Path path ) {
        try {
            this.root = FXMLLoader.load( path.getPath() );

            Scene scene = new Scene(this.root);

            this.stage.setScene(scene);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public Parent getRoot() {
        return root;
    }

    public void setRoot(Parent root) { this.root = root; }
    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
