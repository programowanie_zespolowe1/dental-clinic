package main.java.navigator;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public enum Path {
    title("src/main/java/GUI/title/title.fxml"),
    login("src/main/java/GUI/login/login.fxml"),
    landing("src/main/java/GUI/landing/landing.fxml"),
    patientCard("src/main/java/GUI/patientCard/patientCard.fxml"),
    upperTeeth("src/main/java/GUI/patientCard/upperTeeth.fxml"),
    lowerTeeth("src/main/java/GUI/patientCard/lowerTeeth.fxml"),
    patientList("src/main/java/GUI/patientList/patientList.fxml"),
    addPatient("src/main/java/GUI/addPatient/addPatient.fxml"),
    editPatient("src/main/java/GUI/editPatient/editPatient.fxml"),
    calendar("src/main/java/GUI/calendar/calendar.fxml"),
    addUser("src/main/java/GUI/adminAddUser/addUser.fxml"),
    userList("src/main/java/GUI/adminUserList/userList.fxml"),
    userDetails("src/main/java/GUI/adminUserDetails/userDetails.fxml"),
    appointmentList("src/main/java/GUI/appointmentList/appointmentList.fxml"),
    makeAppointment("src/main/java/GUI/makeAppointment/makeAppointment.fxml"),
    editDetails("src/main/java/GUI/userInfo/editDetails.fxml"),
    editTimetable("src/main/java/GUI/editSchedule/editTimetable.fxml"),
    userInfo("src/main/java/GUI/userInfo/userInfo.fxml"),
    addToothHistory("src/main/java/GUI/addToothHistory/addToothHistory.fxml"),
    editToothHistory("src/main/java/GUI/editToothHistory/editToothHistory.fxml"),
    editUserInfo("src/main/java/GUI/editUserInfo/editDetails.fxml");

    private final String path;

    Path(final String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return path;
    }

    public URL getPath() throws MalformedURLException {
        return new File(path).toURL();
    }
}
