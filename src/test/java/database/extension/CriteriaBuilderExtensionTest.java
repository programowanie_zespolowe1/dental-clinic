package test.java.database.extension;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.extension.CriteriaBuilderExtension;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

class CriteriaBuilderExtensionTest {

    private static SessionFactory sessionFactory;

    @BeforeAll
    static void setup() {
        sessionFactory = Connector.getSessionFactory();
    }

    @AfterAll
    static void dispose() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }

    @BeforeEach
    void openSession() {
        sessionFactory.openSession();
    }

    @AfterEach
    void closeSession() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }


    @Test
    void getCriteriaQueryTest() {
        var session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        var profile = new Profile("ProfileTest", "ProfileTest", "63050457796",
                "ProfileTestCity", "ProfileTestAddress", "00-000");
        var cbe = new CriteriaBuilderExtension(session.getSession(), profile);

        var q = cbe.getCriteriaQuery();

        Assertions.assertNotNull(q);
        session.getTransaction().rollback();
    }

    @Test
    void getRoot() {
        var session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        var profile = new Profile("ProfileTest", "ProfileTest", "63050457796",
                "ProfileTestCity", "ProfileTestAddress", "00-000");
        var cbe = new CriteriaBuilderExtension(session.getSession(), profile);

        var q = cbe.getCriteriaQuery();
        var r = cbe.getRoot();

        Assertions.assertNotNull(r);
        session.getTransaction().rollback();
    }
}