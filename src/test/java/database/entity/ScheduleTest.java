package test.java.database.entity;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.Schedule;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Time;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ScheduleTest {

    private static SessionFactory sessionFactory;

    @BeforeAll
    static void setup() {
        sessionFactory = Connector.getSessionFactory();
    }

    @AfterAll
    static void dispose() { if (sessionFactory != null) sessionFactory.getCurrentSession().close(); }

    @BeforeEach
    void openSession() {
        sessionFactory.openSession();
    }

    @AfterEach
    void closeSession() { if (sessionFactory != null) sessionFactory.getCurrentSession().close(); }

    @Test
    @Order(1)
    public void createScheduleEntryForProfile(){
        Profile testProfile = new Profile("testName", "testLastName", "99887766554", "testCity", "testAddress 1", "00-000", "testMail@example.com", "987654321");
        Schedule testSchedule = new Schedule(Time.valueOf("9:00:00"), Time.valueOf("16:00:00"), "Poniedzialek", testProfile);

        Profile foundProfile;
        Schedule foundSchedule;


        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Schedule> scheduleCriteriaQuery = builder.createQuery(Schedule.class);
        Root<Schedule> scheduleRoot = scheduleCriteriaQuery.from(Schedule.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);


        try{
            currentSession.persist(testProfile);
            currentSession.persist(testSchedule);

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), "99887766554"));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(scheduleRoot.get("startTime"), Time.valueOf("9:00:00"));
            predicates[1] = builder.equal(scheduleRoot.get("endTime"), Time.valueOf("16:00:00"));
            predicates[2] = builder.equal(scheduleRoot.get("dayOfWeek"), "Poniedzialek");
            predicates[3] = builder.equal(scheduleRoot.get("profile"), foundProfile);

            scheduleCriteriaQuery.select(scheduleRoot).where(predicates);
            Query sessionFindScheduleQuery = currentSession.createQuery(scheduleCriteriaQuery);

            foundSchedule = (Schedule) sessionFindScheduleQuery.getSingleResult();

            currentSession.getTransaction().commit();
        } catch(Exception e){
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }


        Assertions.assertEquals(testProfile.getGUID(), foundProfile.getGUID());
        Assertions.assertEquals(testSchedule.getProfile().getGUID(), foundProfile.getGUID());
        Assertions.assertEquals(testSchedule.getId(), foundSchedule.getId());
    }

    @Test
    @Order(2)
    public void updateSchedule() {
        String testPesel = "99887766554";
        Time testStartTime = Time.valueOf("10:00:00");
        Time testEndTime = Time.valueOf("17:00:00");
        Schedule foundNewSchedule;
        Schedule foundOriginalSchedule;
        Profile foundProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Schedule> scheduleCriteriaQuery = builder.createQuery(Schedule.class);
        Root<Schedule> scheduleRoot = scheduleCriteriaQuery.from(Schedule.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        Query sessionFindScheduleQuery;

        try {

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(scheduleRoot.get("startTime"), Time.valueOf("9:00:00"));
            predicates[1] = builder.equal(scheduleRoot.get("endTime"), Time.valueOf("16:00:00"));
            predicates[2] = builder.equal(scheduleRoot.get("dayOfWeek"), "Poniedzialek");
            predicates[3] = builder.equal(scheduleRoot.get("profile"), foundProfile);

            scheduleCriteriaQuery.select(scheduleRoot).where(predicates);
            sessionFindScheduleQuery = currentSession.createQuery(scheduleCriteriaQuery);

            foundOriginalSchedule = (Schedule) sessionFindScheduleQuery.getSingleResult();

            foundOriginalSchedule.setStartTime(testStartTime);
            foundOriginalSchedule.setEndTime(testEndTime);

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();


            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(scheduleRoot.get("startTime"), testStartTime);
            predicates[1] = builder.equal(scheduleRoot.get("endTime"), testEndTime);
            predicates[2] = builder.equal(scheduleRoot.get("dayOfWeek"), "Poniedzialek");
            predicates[3] = builder.equal(scheduleRoot.get("profile"), foundProfile);

            scheduleCriteriaQuery.select(scheduleRoot).where(predicates);
            sessionFindScheduleQuery = currentSession.createQuery(scheduleCriteriaQuery);

            foundNewSchedule = (Schedule) sessionFindScheduleQuery.getSingleResult();

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }


        Assertions.assertEquals(foundOriginalSchedule.getId(), foundNewSchedule.getId());
        Assertions.assertEquals(foundOriginalSchedule.getStartTime(), foundNewSchedule.getStartTime());
        Assertions.assertEquals(foundOriginalSchedule.getEndTime(), foundNewSchedule.getEndTime());
        Assertions.assertEquals(foundNewSchedule.getProfile().getGUID(), foundProfile.getGUID());
        Assertions.assertEquals(foundOriginalSchedule.getProfile().getGUID(), foundProfile.getGUID());
    }

    @Test
    @Order(3)
    public void deleteSchedule() {
        String testPesel = "99887766554";
        Schedule foundNoSchedule;
        Schedule foundOriginalSchedule;
        Profile foundProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Schedule> scheduleCriteriaQuery = builder.createQuery(Schedule.class);
        Root<Schedule> scheduleRoot = scheduleCriteriaQuery.from(Schedule.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        Query sessionFindScheduleQuery;

        try {
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(scheduleRoot.get("startTime"), Time.valueOf("10:00:00"));
            predicates[1] = builder.equal(scheduleRoot.get("endTime"), Time.valueOf("17:00:00"));
            predicates[2] = builder.equal(scheduleRoot.get("dayOfWeek"), "Poniedzialek");
            predicates[3] = builder.equal(scheduleRoot.get("profile"), foundProfile);

            scheduleCriteriaQuery.select(scheduleRoot).where(predicates);
            sessionFindScheduleQuery = currentSession.createQuery(scheduleCriteriaQuery);

            foundOriginalSchedule = (Schedule) sessionFindScheduleQuery.getSingleResult();

            currentSession.delete(foundOriginalSchedule);
            currentSession.getTransaction().commit();

        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(scheduleRoot.get("startTime"), Time.valueOf("10:00:00"));
            predicates[1] = builder.equal(scheduleRoot.get("endTime"), Time.valueOf("17:00:00"));
            predicates[2] = builder.equal(scheduleRoot.get("dayOfWeek"), "Poniedzialek");
            predicates[3] = builder.equal(scheduleRoot.get("profile"), foundProfile);

            scheduleCriteriaQuery.select(scheduleRoot).where(predicates);
            sessionFindScheduleQuery = currentSession.createQuery(scheduleCriteriaQuery);

            try {
                foundNoSchedule = (Schedule) sessionFindScheduleQuery.getSingleResult();
            } catch (NoResultException e) {
                foundNoSchedule = null;
            }

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            currentSession.delete(foundProfile);
            currentSession.getTransaction().commit();

        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            try {
                foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();
            } catch (NoResultException e) {
                foundProfile = null;
            }

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        Assertions.assertNull(foundNoSchedule);
        Assertions.assertNull(foundProfile);
    }


}