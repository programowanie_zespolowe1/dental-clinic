package test.java.database.entity;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ProfileTest {

    private static SessionFactory sessionFactory;

    @BeforeAll
    static void setup() {
        sessionFactory = Connector.getSessionFactory();
    }

    @AfterAll
    static void dispose() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }

    @BeforeEach
    void openSession() {
        sessionFactory.openSession();
    }

    @AfterEach
    void closeSession() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }

    @Test
    @Order(1)
    public void createProfileWithoutUser() {
        Profile testProfile = new Profile("ProfileTest", "ProfileTest", "63050457796", "ProfileTestCity", "ProfileTestAddress", "00-000");
        Profile foundProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {
            currentSession.persist(testProfile);

            CriteriaBuilder builder = currentSession.getCriteriaBuilder();
            CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
            Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

            Predicate[] predicates = new Predicate[6];
            predicates[0] = builder.equal(profileRoot.get("firstName"), "ProfileTest");
            predicates[1] = builder.equal(profileRoot.get("lastName"), "ProfileTest");
            predicates[2] = builder.equal(profileRoot.get("PESEL"), "63050457796");
            predicates[3] = builder.equal(profileRoot.get("city"), "ProfileTestCity");
            predicates[4] = builder.equal(profileRoot.get("address"), "ProfileTestAddress");
            predicates[5] = builder.equal(profileRoot.get("postalCode"), "00-000");

            profileCriteriaQuery.select(profileRoot).where(predicates);
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        Assertions.assertEquals(testProfile.getGUID(), foundProfile.getGUID());
    }

    @Test
    @Order(2)
    public void updateProfileWithoutUser() {
        String testMail = "ProfTest@example.com";
        Profile foundNewProfile;
        Profile foundOriginalProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);
        Query sessionFindProfileQuery;

        try {

            Predicate[] predicates = new Predicate[6];
            predicates[0] = builder.equal(profileRoot.get("firstName"), "ProfileTest");
            predicates[1] = builder.equal(profileRoot.get("lastName"), "ProfileTest");
            predicates[2] = builder.equal(profileRoot.get("PESEL"), "63050457796");
            predicates[3] = builder.equal(profileRoot.get("city"), "ProfileTestCity");
            predicates[4] = builder.equal(profileRoot.get("address"), "ProfileTestAddress");
            predicates[5] = builder.equal(profileRoot.get("postalCode"), "00-000");

            profileCriteriaQuery.select(profileRoot).where(predicates);
            sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundOriginalProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            foundOriginalProfile.setEmail(testMail);

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("email"), testMail));
            sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundNewProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        Assertions.assertEquals(foundOriginalProfile.getGUID(), foundNewProfile.getGUID());
        Assertions.assertEquals(foundOriginalProfile.getEmail(), foundNewProfile.getEmail());
    }

    @Test
    @Order(3)
    public void deleteProfileWithoutUser() {
        String testMail = "ProfTest@example.com";
        Profile foundNoProfile;
        Profile foundOriginalProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);
        Query sessionFindProfileQuery;

        try {
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("email"), testMail));
            sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundOriginalProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            currentSession.delete(foundOriginalProfile);
            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {
            Predicate[] predicates = new Predicate[6];
            predicates[0] = builder.equal(profileRoot.get("firstName"), "ProfileTest");
            predicates[1] = builder.equal(profileRoot.get("lastName"), "ProfileTest");
            predicates[2] = builder.equal(profileRoot.get("PESEL"), "63050457796");
            predicates[3] = builder.equal(profileRoot.get("city"), "ProfileTestCity");
            predicates[4] = builder.equal(profileRoot.get("address"), "ProfileTestAddress");
            predicates[5] = builder.equal(profileRoot.get("postalCode"), "00-000");

            profileCriteriaQuery.select(profileRoot).where(predicates);
            sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            try {
                foundNoProfile = (Profile) sessionFindProfileQuery.getSingleResult();
            } catch (NoResultException e) {
                foundNoProfile = null;
            }

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        Assertions.assertNull(foundNoProfile);
    }
}
