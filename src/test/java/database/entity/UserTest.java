package test.java.database.entity;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.User;
import main.java.database.enums.Role;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    private static SessionFactory sessionFactory;

    @BeforeAll
    static void setup() {
        sessionFactory = Connector.getSessionFactory();
    }

    @AfterAll
    static void dispose() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }

    @BeforeEach
    void openSession() {
        sessionFactory.openSession();
    }

    @AfterEach
    void closeSession() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }

    @Test
    @Order(1)
    public void createUserWithProfile() {
        Profile testProfile = new Profile("ProfileTest", "ProfileTest", "11223344556", "ProfileTestCity", "ProfileTestAddress", "00-000");
        User testUser = new User("testName", "testPassword", Role.Dentysta, testProfile);
        Profile foundProfile;
        User foundUser;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<User> userCriteriaQuery = builder.createQuery(User.class);
        Root<User> userRoot = userCriteriaQuery.from(User.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        try{
            currentSession.persist(testProfile);
            currentSession.persist(testUser);

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), "11223344556"));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(userRoot.get("userName"), "testName");
            predicates[1] = builder.equal(userRoot.get("password"), "testPassword");
            predicates[2] = builder.equal(userRoot.get("role"), Role.Dentysta);
            predicates[3] = builder.equal(userRoot.get("profile"), foundProfile);

            userCriteriaQuery.select(userRoot).where(predicates);
            Query sessionFindUserQuery = currentSession.createQuery(userCriteriaQuery);

            foundUser = (User) sessionFindUserQuery.getSingleResult();

            currentSession.getTransaction().commit();
        } catch(Exception e){
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }


        Assertions.assertEquals(testProfile.getGUID(), foundProfile.getGUID());
        Assertions.assertEquals(testUser.getGUID(), foundUser.getGUID());
        Assertions.assertEquals(testUser.getProfile().getGUID(), foundProfile.getGUID());
    }

    @Test
    @Order(2)
    public void updateUserWithProile() {
        String testPesel = "11223344556";
        String testUserName = "testName2";
        String testPassword = "testPassword2";
        User foundNewUser;
        User foundOriginalUser;
        Profile foundProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<User> userCriteriaQuery = builder.createQuery(User.class);
        Root<User> userRoot = userCriteriaQuery.from(User.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        Query sessionFindUserQuery;

        try {

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(userRoot.get("userName"), "testName");
            predicates[1] = builder.equal(userRoot.get("password"), "testPassword");
            predicates[2] = builder.equal(userRoot.get("role"), Role.Dentysta);
            predicates[3] = builder.equal(userRoot.get("profile"), foundProfile);

            userCriteriaQuery.select(userRoot).where(predicates);
            sessionFindUserQuery = currentSession.createQuery(userCriteriaQuery);

            foundOriginalUser = (User) sessionFindUserQuery.getSingleResult();

            foundOriginalUser.setUserName(testUserName);
            foundOriginalUser.setPassword(testPassword);

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();


            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(userRoot.get("userName"), testUserName);
            predicates[1] = builder.equal(userRoot.get("password"), testPassword);
            predicates[2] = builder.equal(userRoot.get("role"), Role.Dentysta);
            predicates[3] = builder.equal(userRoot.get("profile"), foundProfile);

            userCriteriaQuery.select(userRoot).where(predicates);
            sessionFindUserQuery = currentSession.createQuery(userCriteriaQuery);

            foundNewUser = (User) sessionFindUserQuery.getSingleResult();

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        Assertions.assertEquals(foundOriginalUser.getGUID(), foundNewUser.getGUID());
        Assertions.assertEquals(foundOriginalUser.getUserName(), foundNewUser.getUserName());
        Assertions.assertEquals(foundOriginalUser.getPassword(), foundNewUser.getPassword());
        Assertions.assertEquals(foundNewUser.getProfile().getGUID(), foundProfile.getGUID());
        Assertions.assertEquals(foundOriginalUser.getProfile().getGUID(), foundProfile.getGUID());
    }

    @Test
    @Order(3)
    public void deleteUserWithProfile() {
        String testPesel = "11223344556";
        String testUserName = "testName2";
        String testPassword = "testPassword2";
        User foundNoUser;
        User foundOriginalUser;
        Profile foundProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<User> userCriteriaQuery = builder.createQuery(User.class);
        Root<User> userRoot = userCriteriaQuery.from(User.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        Query sessionFindUserQuery;

        try {
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(userRoot.get("userName"), testUserName);
            predicates[1] = builder.equal(userRoot.get("password"), testPassword);
            predicates[2] = builder.equal(userRoot.get("role"), Role.Dentysta);
            predicates[3] = builder.equal(userRoot.get("profile"), foundProfile);

            userCriteriaQuery.select(userRoot).where(predicates);
            sessionFindUserQuery = currentSession.createQuery(userCriteriaQuery);

            foundOriginalUser = (User) sessionFindUserQuery.getSingleResult();

            currentSession.delete(foundOriginalUser);
            currentSession.getTransaction().commit();

        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(userRoot.get("userName"), testUserName);
            predicates[1] = builder.equal(userRoot.get("password"), testPassword);
            predicates[2] = builder.equal(userRoot.get("role"), Role.Dentysta);
            predicates[3] = builder.equal(userRoot.get("profile"), foundProfile);

            userCriteriaQuery.select(userRoot).where(predicates);
            sessionFindUserQuery = currentSession.createQuery(userCriteriaQuery);

            try {
                foundNoUser = (User) sessionFindUserQuery.getSingleResult();
            } catch (NoResultException e) {
                foundNoUser = null;
            }

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();

            currentSession.delete(foundProfile);
            currentSession.getTransaction().commit();

        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

            try {
                foundProfile = (Profile) sessionFindProfileQuery.getSingleResult();
            } catch (NoResultException e) {
                foundProfile = null;
            }

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        Assertions.assertNull(foundNoUser);
        Assertions.assertNull(foundProfile);
    }


}