package test.java.database.entity;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.ToothHistory;
import main.java.database.entity.Visit;
import main.java.database.enums.ToothState;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Date;

// CRUD

// CreateToothHistoryForPatient
// UpdateToothHistoryForPatient
// DeleteToothHistoryForPatient


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ToothHistoryTest {

    private static SessionFactory sessionFactory;

    @BeforeAll
    static void setup() {
        sessionFactory = Connector.getSessionFactory();
    }

    @AfterAll
    static void dispose() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }

    @BeforeEach
    void openSession() {
        sessionFactory.openSession();
    }

    @AfterEach
    void closeSession() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }

    @Test
    @Order(1)
    public void createToothHistoryForPatient() {
        Profile testProfile = new Profile("testName", "testLastName", "99887766554", "testCity", "testAddress 1", "00-000", "testMail@example.com", "987654321");
        ToothHistory testToothHistory = new ToothHistory(1, Date.valueOf("2021-09-03"), "testHistory", "testXRayPicture", ToothState.NONE, testProfile);

        ToothHistory foundToothHistory;
        Profile foundProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<ToothHistory> toothHistoryCriteriaQuery = builder.createQuery(ToothHistory.class);
        Root<ToothHistory> toothHistoryRoot = toothHistoryCriteriaQuery.from(ToothHistory.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        try{
            currentSession.persist(testProfile);
            currentSession.persist(testToothHistory);

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), "99887766554"));
            Query sessionFindPatientQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindPatientQuery.getSingleResult();

            Predicate[] predicates = new Predicate[6];
            predicates[0] = builder.equal(toothHistoryRoot.get("toothNumber"), 1);
            predicates[1] = builder.equal(toothHistoryRoot.get("visitDate"), Date.valueOf("2021-09-03"));
            predicates[2] = builder.equal(toothHistoryRoot.get("history"), "testHistory");
            predicates[3] = builder.equal(toothHistoryRoot.get("xRayPicture"), "testXRayPicture");
            predicates[4] = builder.equal(toothHistoryRoot.get("toothState"), ToothState.NONE);
            predicates[5] = builder.equal(toothHistoryRoot.get("profile"), foundProfile);

            toothHistoryCriteriaQuery.select(toothHistoryRoot).where(predicates);
            Query sessionFindToothHistoryQuery = currentSession.createQuery(toothHistoryCriteriaQuery);

            foundToothHistory = (ToothHistory) sessionFindToothHistoryQuery.getSingleResult();

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();
            throw e;
        } finally {
            currentSession.close();
        }
        Assertions.assertEquals(testProfile.getGUID(), foundProfile.getGUID());
        Assertions.assertEquals(testToothHistory.getProfile().getGUID(), foundProfile.getGUID());
        Assertions.assertEquals(testToothHistory.getId(), foundToothHistory.getId());
    }

    @Test
    @Order(2)
    public void updateToothHistoryForPatient() {
        String testPesel = "99887766554";
        Date testDate = Date.valueOf("1997-01-01");

        ToothHistory foundNewToothHistory;
        ToothHistory foundOriginalToothHistory;
        Profile foundProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<ToothHistory> toothHistoryCriteriaQuery = builder.createQuery(ToothHistory.class);
        Root<ToothHistory> toothHistoryRoot = toothHistoryCriteriaQuery.from(ToothHistory.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        Query sessionFindToothHistoryQuery;

        try {

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindPatientQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindPatientQuery.getSingleResult();

            Predicate[] predicates = new Predicate[6];
            predicates[0] = builder.equal(toothHistoryRoot.get("toothNumber"), 1);
            predicates[1] = builder.equal(toothHistoryRoot.get("visitDate"), Date.valueOf("2021-09-03"));
            predicates[2] = builder.equal(toothHistoryRoot.get("history"), "testHistory");
            predicates[3] = builder.equal(toothHistoryRoot.get("xRayPicture"), "testXRayPicture");
            predicates[4] = builder.equal(toothHistoryRoot.get("toothState"), ToothState.NONE);
            predicates[5] = builder.equal(toothHistoryRoot.get("profile"), foundProfile);

            toothHistoryCriteriaQuery.select(toothHistoryRoot).where(predicates);
            sessionFindToothHistoryQuery = currentSession.createQuery(toothHistoryCriteriaQuery);

            foundOriginalToothHistory = (ToothHistory) sessionFindToothHistoryQuery.getSingleResult();

            foundOriginalToothHistory.setVisitDate(testDate);

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();
            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try{
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), "99887766554"));
            Query sessionFindPatientQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindPatientQuery.getSingleResult();

            Predicate[] predicates = new Predicate[6];
            predicates[0] = builder.equal(toothHistoryRoot.get("toothNumber"), 1);
            predicates[1] = builder.equal(toothHistoryRoot.get("visitDate"), testDate);
            predicates[2] = builder.equal(toothHistoryRoot.get("history"), "testHistory");
            predicates[3] = builder.equal(toothHistoryRoot.get("xRayPicture"), "testXRayPicture");
            predicates[4] = builder.equal(toothHistoryRoot.get("toothState"), ToothState.NONE);
            predicates[5] = builder.equal(toothHistoryRoot.get("profile"), foundProfile);

            toothHistoryCriteriaQuery.select(toothHistoryRoot).where(predicates);
            sessionFindToothHistoryQuery = currentSession.createQuery(toothHistoryCriteriaQuery);

            foundNewToothHistory = (ToothHistory) sessionFindToothHistoryQuery.getSingleResult();


            currentSession.getTransaction().commit();
        }
        catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        Assertions.assertEquals(foundOriginalToothHistory.getId(), foundNewToothHistory.getId());
        Assertions.assertEquals(foundOriginalToothHistory.getVisitDate(), foundNewToothHistory.getVisitDate());
        Assertions.assertEquals(foundNewToothHistory.getProfile().getGUID(), foundProfile.getGUID());
        Assertions.assertEquals(foundOriginalToothHistory.getProfile().getGUID(), foundProfile.getGUID());


    }

    @Test
    @Order(3)
    public void deleteToothHistoryForPatient() {
        String testPesel = "99887766554";
        Date testDate = Date.valueOf("1997-01-01");

        ToothHistory foundNoToothHistory;
        ToothHistory foundOriginalToothHistory;
        Profile foundProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<ToothHistory> toothHistoryCriteriaQuery = builder.createQuery(ToothHistory.class);
        Root<ToothHistory> toothHistoryRoot = toothHistoryCriteriaQuery.from(ToothHistory.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        Query sessionFindToothHistoryQuery;

        try {

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindPatientQuery = currentSession.createQuery(profileCriteriaQuery);

            foundProfile = (Profile) sessionFindPatientQuery.getSingleResult();

            Predicate[] predicates = new Predicate[6];
            predicates[0] = builder.equal(toothHistoryRoot.get("toothNumber"), 1);
            predicates[1] = builder.equal(toothHistoryRoot.get("visitDate"), testDate);
            predicates[2] = builder.equal(toothHistoryRoot.get("history"), "testHistory");
            predicates[3] = builder.equal(toothHistoryRoot.get("xRayPicture"), "testXRayPicture");
            predicates[4] = builder.equal(toothHistoryRoot.get("toothState"), ToothState.NONE);
            predicates[5] = builder.equal(toothHistoryRoot.get("profile"), foundProfile);

            toothHistoryCriteriaQuery.select(toothHistoryRoot).where(predicates);
            sessionFindToothHistoryQuery = currentSession.createQuery(toothHistoryCriteriaQuery);

            foundOriginalToothHistory = (ToothHistory) sessionFindToothHistoryQuery.getSingleResult();

            currentSession.delete(foundOriginalToothHistory);
            currentSession.delete(foundProfile);
            currentSession.getTransaction().commit();

        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();
            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try{
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPesel));
            Query sessionFindPatientQuery = currentSession.createQuery(profileCriteriaQuery);

            try {
                foundProfile = (Profile) sessionFindPatientQuery.getSingleResult();
            } catch(NoResultException e) {
                foundProfile = null;
            }

            Predicate[] predicates = new Predicate[6];
            predicates[0] = builder.equal(toothHistoryRoot.get("toothNumber"), 1);
            predicates[1] = builder.equal(toothHistoryRoot.get("visitDate"), testDate);
            predicates[2] = builder.equal(toothHistoryRoot.get("history"), "testHistory");
            predicates[3] = builder.equal(toothHistoryRoot.get("xRayPicture"), "testXRayPicture");
            predicates[4] = builder.equal(toothHistoryRoot.get("toothState"), ToothState.NONE);
            predicates[5] = builder.equal(toothHistoryRoot.get("profile"), foundProfile);

            toothHistoryCriteriaQuery.select(toothHistoryRoot).where(predicates);
            sessionFindToothHistoryQuery = currentSession.createQuery(toothHistoryCriteriaQuery);

            try {
                foundNoToothHistory = (ToothHistory) sessionFindToothHistoryQuery.getSingleResult();
            } catch (NoResultException e) {
                foundNoToothHistory = null;
            }

            currentSession.getTransaction().commit();
        }
        catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        Assertions.assertNull(foundProfile);
        Assertions.assertNull(foundNoToothHistory);

    }

}


