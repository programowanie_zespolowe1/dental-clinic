package test.java.database.entity;

import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.Visit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.LocalTime;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class VisitTest {

    private static SessionFactory sessionFactory;

    @BeforeAll
    static void setup() {
        sessionFactory = Connector.getSessionFactory();
    }

    @AfterAll
    static void dispose() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }

    @BeforeEach
    void openSession() {
        sessionFactory.openSession();
    }

    @AfterEach
    void closeSession() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }

    @Test
    @Order(1)
    public void createVisit() {
        Profile testDentistProfile = new Profile("ProfileTest", "ProfileTest", "63050457796",
                "ProfileTestCity", "ProfileTestAddress", "00-000");
        Profile testPatientProfile = new Profile("ProfileTest1", "ProfileTest1", "12345678910",
                "ProfileTestCity1", "ProfileTestAddress1", "11-111");
        Visit testVisit = new Visit(LocalDate.of(2018,9,1), LocalTime.of(9,00), testDentistProfile, testPatientProfile);

        Profile foundDentistProfile;
        Profile foundPatientProfile;
        Visit foundVisit;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Visit> visitCriteriaQuery = builder.createQuery(Visit.class);
        Root<Visit> visitRoot = visitCriteriaQuery.from(Visit.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        try {
            currentSession.persist(testDentistProfile);
            currentSession.persist(testPatientProfile);
            currentSession.persist(testVisit);

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), "63050457796"));
            Query sessionFindDentistQuery = currentSession.createQuery(profileCriteriaQuery);

            foundDentistProfile = (Profile) sessionFindDentistQuery.getSingleResult();

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), "12345678910"));
            Query sessionFindPatientQuery = currentSession.createQuery(profileCriteriaQuery);

            foundPatientProfile = (Profile) sessionFindPatientQuery.getSingleResult();

            Predicate[] predicates = new Predicate[4];
            predicates[0] = builder.equal(visitRoot.get("visitDate"), LocalDate.of(2018,9,1));
            predicates[1] = builder.equal(visitRoot.get("visitTime"), LocalTime.of(9,00));
            predicates[2] = builder.equal(visitRoot.get("dentistProfile"), foundDentistProfile);
            predicates[3] = builder.equal(visitRoot.get("patientProfile"), foundPatientProfile);

            visitCriteriaQuery.select(visitRoot).where(predicates);
            Query sessionFindVisitQuery = currentSession.createQuery(visitCriteriaQuery);

            foundVisit = (Visit) sessionFindVisitQuery.getSingleResult();

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        Assertions.assertEquals(testDentistProfile.getGUID(), foundDentistProfile.getGUID());
        Assertions.assertEquals(testPatientProfile.getGUID(), foundPatientProfile.getGUID());
        Assertions.assertEquals(testVisit.getId(), foundVisit.getId());
        Assertions.assertEquals(testVisit.getDentistProfile().getGUID(), foundVisit.getDentistProfile().getGUID());
        Assertions.assertEquals(testVisit.getPatientProfile().getGUID(), foundVisit.getPatientProfile().getGUID());
    }

    @Test
    @Order(2)
    public void updateVisit() {
        String testPeselDentist = "63050457796";
        String testPeselPatient = "12345678910";
        LocalDate testDate = LocalDate.of(2021,4,4);
        LocalTime testTime = LocalTime.of(9,00);
        Visit foundNewVisit;
        Visit foundOriginalVisit;

        Profile foundDentistProfile;
        Profile foundPatientProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Visit> visitCriteriaQuery = builder.createQuery(Visit.class);
        Root<Visit> visitRoot = visitCriteriaQuery.from(Visit.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        Query sessionFindVisitQuery;

        try {

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPeselDentist));
            Query sessionFindDentistQuery = currentSession.createQuery(profileCriteriaQuery);

            foundDentistProfile = (Profile) sessionFindDentistQuery.getSingleResult();

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPeselPatient));
            Query sessionFindPatientQuery = currentSession.createQuery(profileCriteriaQuery);

            foundPatientProfile = (Profile) sessionFindPatientQuery.getSingleResult();

            Predicate[] predicates = new Predicate[3];
            predicates[0] = builder.equal(visitRoot.get("visitDate"), LocalDate.of(2018, 9,1));
            predicates[1] = builder.equal(visitRoot.get("visitTime"), LocalTime.of(9,00));
            predicates[1] = builder.equal(visitRoot.get("dentistProfile"), foundDentistProfile);
            predicates[2] = builder.equal(visitRoot.get("patientProfile"), foundPatientProfile);

            visitCriteriaQuery.select(visitRoot).where(predicates);
            sessionFindVisitQuery = currentSession.createQuery(visitCriteriaQuery);

            foundOriginalVisit = (Visit) sessionFindVisitQuery.getSingleResult();

            foundOriginalVisit.setVisitDate(testDate);

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), "63050457796"));
            Query sessionFindDentistQuery = currentSession.createQuery(profileCriteriaQuery);

            foundDentistProfile = (Profile) sessionFindDentistQuery.getSingleResult();

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), "12345678910"));
            Query sessionFindPatientQuery = currentSession.createQuery(profileCriteriaQuery);

            foundPatientProfile = (Profile) sessionFindPatientQuery.getSingleResult();

            Predicate[] predicates = new Predicate[3];
            predicates[0] = builder.equal(visitRoot.get("visitDate"), testDate);
            predicates[1] = builder.equal(visitRoot.get("dentistProfile"), foundDentistProfile);
            predicates[2] = builder.equal(visitRoot.get("patientProfile"), foundPatientProfile);

            visitCriteriaQuery.select(visitRoot).where(predicates);
            sessionFindVisitQuery = currentSession.createQuery(visitCriteriaQuery);

            foundNewVisit = (Visit) sessionFindVisitQuery.getSingleResult();

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        Assertions.assertEquals(foundOriginalVisit.getId(), foundNewVisit.getId());
        Assertions.assertEquals(foundOriginalVisit.getVisitDate(), foundNewVisit.getVisitDate());
        Assertions.assertEquals(foundNewVisit.getDentistProfile().getGUID(), foundDentistProfile.getGUID());
        Assertions.assertEquals(foundOriginalVisit.getDentistProfile().getGUID(), foundDentistProfile.getGUID());
        Assertions.assertEquals(foundNewVisit.getPatientProfile().getGUID(), foundPatientProfile.getGUID());
        Assertions.assertEquals(foundOriginalVisit.getPatientProfile().getGUID(), foundPatientProfile.getGUID());

    }

    @Test
    @Order(3)
    public void deleteVisit() {
        String testPeselDentist = "63050457796";
        String testPeselPatient = "12345678910";
        LocalDate testDate = LocalDate.of(2021,4,4);
        LocalTime testTime = LocalTime.of(9,00);
        Visit foundNoVisit;
        Visit foundOriginalVisit;
        Profile foundDentistProfile;
        Profile foundPatientProfile;

        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<Visit> visitCriteriaQuery = builder.createQuery(Visit.class);
        Root<Visit> visitRoot = visitCriteriaQuery.from(Visit.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        Query sessionFindVisitQuery;

        try {
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPeselDentist));
            Query sessionFindDentistQuery = currentSession.createQuery(profileCriteriaQuery);

            foundDentistProfile = (Profile) sessionFindDentistQuery.getSingleResult();

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPeselPatient));
            Query sessionFindPatientQuery = currentSession.createQuery(profileCriteriaQuery);

            foundPatientProfile = (Profile) sessionFindPatientQuery.getSingleResult();

            Predicate[] predicates = new Predicate[3];
            predicates[0] = builder.equal(visitRoot.get("visitDate"), testDate);
            predicates[1] = builder.equal(visitRoot.get("dentistProfile"), foundDentistProfile);
            predicates[2] = builder.equal(visitRoot.get("patientProfile"), foundPatientProfile);

            visitCriteriaQuery.select(visitRoot).where(predicates);
            sessionFindVisitQuery = currentSession.createQuery(visitCriteriaQuery);

            foundOriginalVisit = (Visit) sessionFindVisitQuery.getSingleResult();

            currentSession.delete(foundOriginalVisit);
            currentSession.delete(foundDentistProfile);
            currentSession.delete(foundPatientProfile);
            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        try {
            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPeselDentist));
            Query sessionFindDentistQuery = currentSession.createQuery(profileCriteriaQuery);

            try {
                foundDentistProfile = (Profile) sessionFindDentistQuery.getSingleResult();
            }catch (NoResultException e) {
                foundDentistProfile = null;
            }

            profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("PESEL"), testPeselPatient));
            Query sessionFindPatientQuery = currentSession.createQuery(profileCriteriaQuery);

            try {
                foundPatientProfile = (Profile) sessionFindPatientQuery.getSingleResult();
            }catch (NoResultException e){
                foundPatientProfile = null;
            }

            Predicate[] predicates = new Predicate[3];
            predicates[0] = builder.equal(visitRoot.get("visitDate"), testDate);
            predicates[1] = builder.equal(visitRoot.get("dentistProfile"), foundDentistProfile);
            predicates[2] = builder.equal(visitRoot.get("patientProfile"), foundPatientProfile);

            visitCriteriaQuery.select(visitRoot).where(predicates);
            sessionFindVisitQuery = currentSession.createQuery(visitCriteriaQuery);

            try {
                foundNoVisit = (Visit) sessionFindVisitQuery.getSingleResult();
            } catch (NoResultException e) {
                foundNoVisit = null;
            }

            currentSession.getTransaction().commit();
        } catch (Exception e) {
            if (currentSession.getTransaction() != null)
                currentSession.getTransaction().rollback();

            throw e;
        } finally {
            currentSession.close();
        }

        Assertions.assertNull(foundPatientProfile);
        Assertions.assertNull(foundDentistProfile);
        Assertions.assertNull(foundNoVisit);
    }

}