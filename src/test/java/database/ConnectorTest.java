package test.java.database;


import main.java.database.Connector;
import main.java.database.entity.Profile;
import main.java.database.entity.User;
import main.java.database.enums.Role;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

class ConnectorTest {

    private static SessionFactory sessionFactory;

    @BeforeAll
    static void setup() {
        sessionFactory = Connector.getSessionFactory();
    }

    @AfterAll
    static void dispose() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }

    @BeforeEach
    void openSession() {
        sessionFactory.openSession();
    }

    @AfterEach
    void closeSession() {
        if (sessionFactory != null) sessionFactory.getCurrentSession().close();
    }

    @Test
    void isSessionOpened() {
        Session currentSession = sessionFactory.getCurrentSession();

        Assertions.assertTrue(currentSession.isOpen());
    }

    @Test
    void doesSessionExecuteQuery() {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        Profile testProfile = new Profile("Test", "Test", "63050457796", "TestCity", "TestAddress", "00-000");
        User testUser = new User("Test", "Test", Role.Pacjent, testProfile);

        currentSession.persist(testProfile);
        currentSession.persist(testUser);

        currentSession.getTransaction().commit();

        sessionFactory.openSession();
        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        CriteriaBuilder builder = currentSession.getCriteriaBuilder();
        CriteriaQuery<User> userCriteriaQuery = builder.createQuery(User.class);
        Root<User> userRoot = userCriteriaQuery.from(User.class);
        CriteriaQuery<Profile> profileCriteriaQuery = builder.createQuery(Profile.class);
        Root<Profile> profileRoot = profileCriteriaQuery.from(Profile.class);

        userCriteriaQuery.select(userRoot).where(builder.equal(userRoot.get("userName"), "Test"));
        Query sessionFindUserQuery = currentSession.createQuery(userCriteriaQuery);

        profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("firstName"), "Test"));
        Query sessionFindProfileQuery = currentSession.createQuery(profileCriteriaQuery);

        User testUserFind = (User) sessionFindUserQuery.getSingleResult();
        Profile testProfileFind = (Profile) sessionFindProfileQuery.getSingleResult();

        Assertions.assertEquals(testProfile.getGUID(), testProfileFind.getGUID());
        Assertions.assertEquals(testUser.getGUID(), testUserFind.getGUID());

        currentSession.delete(testUserFind);
        currentSession.delete(testProfileFind);

        currentSession.getTransaction().commit();

        sessionFactory.openSession();
        currentSession = sessionFactory.getCurrentSession();
        currentSession.beginTransaction();

        userCriteriaQuery.select(userRoot).where(builder.equal(userRoot.get("userName"), "Test"));
        Query sessionDeletedUserQuery = currentSession.createQuery(userCriteriaQuery);

        profileCriteriaQuery.select(profileRoot).where(builder.equal(profileRoot.get("firstName"), "Test"));
        Query sessionDeletedProfileQuery = currentSession.createQuery(profileCriteriaQuery);

        User testDeletedUser;

        try {
            testDeletedUser = (User) sessionDeletedUserQuery.getSingleResult();
        } catch (NoResultException e) {
            testDeletedUser = null;
        }

        Profile testDeletedProfile;

        try {
            testDeletedProfile = (Profile) sessionDeletedProfileQuery.getSingleResult();
        } catch (NoResultException e) {
            testDeletedProfile = null;
        }

        currentSession.getTransaction().commit();

        Assertions.assertNull(testDeletedUser);
        Assertions.assertNull(testDeletedProfile);
    }
}
